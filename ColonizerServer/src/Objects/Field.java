
package Objects;


import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class Field extends _Object {
    private static int nextID = 1;
    private Position pos;

    private boolean belegt;
    
    public Field(Position pos, boolean belegt) {
        super(nextID);
        this.pos = pos;
        this.belegt = belegt;
        nextID++;
    }
    
    public Field(int fieldID, Position pos, boolean belegt) {
        super(fieldID);
        this.pos = pos;
        this.belegt = belegt;
        nextID = fieldID+1;
    }
    
    
    public static void setNextID(int i) {
        nextID = i;
    }
    
    
    public Position getPosition(){
        return pos;
    }
    
    public boolean getBelegt() {
        return belegt;
    }
    
    public void setBelegt(boolean b) {
        belegt = b;
    }
    
}
