
package Objects;


/**
 * @author Michael Müller
 */


public class Account extends _Object {
    
    private static int nextID = 1;
    private final String name;
    private String pwHash;
    private String sessionHash;
    
    
    //Constructors
    public Account(String name, String password) {
        super(nextID);
        this.name = name;
        this.pwHash = password;
        nextID++;
    }
    
    public Account(int accountID, String name, String password) {
        super(accountID);
        this.name = name;
        this.pwHash = password;
        nextID = accountID+1;
    }
    
    
    public static void setNextID(int i) {
        nextID = i;
    }
    
    public String getName() {
        return name;
    }
    
    public String getPwHash() {
        return pwHash;
    }
    
    public String getSessionHash() {
        return sessionHash;
    }
    
    public void setPassword(String s) {
        this.pwHash = s;
    }
    
    public void setSessionHash(String s) {
        this.sessionHash = s;
    }
}
