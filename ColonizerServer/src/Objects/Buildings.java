
package Objects;


import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class Buildings extends _Object {
        
    private static int nextID = 1;
    
    private int[] buildingLevelsList = new int[9];
    private double tempo;

    //Constructors
    public Buildings(double tempo) {
        super(nextID);
        for (int i = 0; i<9; i++) {
            buildingLevelsList[i] = 1;
        }
        this.tempo = tempo;
        nextID++;
    }
    
    public Buildings(int[] list) {
        super(nextID);
        for (int i = 0; i<9; i++) {
            buildingLevelsList[i] = list[i];
        }
        nextID++;
    }
            
    public Buildings(int objectID, int[] list) {
        super(objectID);
        for (int i = 0; i<9; i++) {
            buildingLevelsList[i] = list[i];
        }
        nextID = objectID++;
    }
    
    
    /**
     * Liefert die Liste mit den Stufen der Gebäude.
     * @return buildingLevelsList
     */
    public int[] getLevelList() {
        return buildingLevelsList;
    }
    
    /**
     * Liefert den Namen des Gebäudes mit der entsprechenden (festgelegten) Nummer.
     * @param buildingNr
     * @return 
     */
    public static String getBuildingNameWithNr(int buildingNr) {
        return buildingList[buildingNr];
    }
    
    /**
     * Rüstet das entsprechende Gebäude auf. 
     * @param buildingNr
     */
    public void upgradeBuilding(int buildingNr) {
        buildingLevelsList[buildingNr] = buildingLevelsList[buildingNr]+1;
    }
    
    /**
     * Liefert die Ausbaukosten des gewünschten Gebäudes.
     * Formel:
     * Kosten = Grundausbaukosten * Ausbaukostenfaktor^Gebäudestufe
     * => Gebäude werden immer teurer...
     * @param buildingNr
     * @return 
     */
    public Resource getUpgradeCosts(int buildingNr) {
        Resource costs = new Resource();
        costs.mineral = -buildingProperties[buildingNr][0]*Math.pow(ausbaukostenfaktor, buildingLevelsList[buildingNr]);
        costs.treibstoff = -buildingProperties[buildingNr][1]*Math.pow(ausbaukostenfaktor, buildingLevelsList[buildingNr]);
        costs.baustoff = -buildingProperties[buildingNr][2]*Math.pow(ausbaukostenfaktor, buildingLevelsList[buildingNr]);
        return costs;
    }
    
    /**
     * Liefert die Ausbaukosten des gewünschten Gebäudes und Level.
     * @param buildingNr
     * @param level
     * @return 
     */
    public Resource getUpgradeCosts(int buildingNr, int level) {
        Resource costs = new Resource();
        costs.mineral = -buildingProperties[buildingNr][0]*Math.pow(ausbaukostenfaktor, level);
        costs.treibstoff = -buildingProperties[buildingNr][1]*Math.pow(ausbaukostenfaktor, level);
        costs.baustoff = -buildingProperties[buildingNr][2]*Math.pow(ausbaukostenfaktor, level);
        return costs;
    }
    
    /**
     * Liefert die benötigte Ausbauzeit für das entsprechende Gebäude (in Millisekunden).
     * @param buildingNr
     * @return 
     */
    public long getUpgradeDuration(int buildingNr) {
        double duration = buildingProperties[buildingNr][3];
        duration = duration * Math.pow(ausbauzeitfaktor, buildingLevelsList[buildingNr]);  
        duration = duration * Math.pow(buildingProperties[0][4], buildingLevelsList[0]-1);
        duration = duration / tempo;
        return Math.round(duration*1000); //*1000 (-> Millisekunden) und Runden
    }
    
    public long getUpgradeDuration(int buildingNr, int level) {
        double duration = buildingProperties[buildingNr][3];
        duration = duration * Math.pow(ausbauzeitfaktor, level);  
        duration = duration * Math.pow(buildingProperties[0][4], buildingLevelsList[0]-1);
        duration = duration / tempo;
        return Math.round(duration*1000); //*1000 (-> Millisekunden) und Runden
    }
    
    public int getMaxStockCapacity() {
        double c = buildingProperties[4][4] * Math.pow(buildingProperties[4][5], buildingLevelsList[4]-1);
        return round(c);
    }
    
    /**
     * Liefert die Rohstoffproduktion pro Sekunde.
     * @return 
     */
    public Resource getResourceProducedPerSecond() {
        Resource production = new Resource();
        production.mineral = grundproduktionProSekunde * Math.pow(produktionsfaktor,buildingLevelsList[1]-1) * tempo;
        production.treibstoff = grundproduktionProSekunde * Math.pow(produktionsfaktor,buildingLevelsList[2]-1) * tempo;
        production.baustoff = grundproduktionProSekunde * Math.pow(produktionsfaktor,buildingLevelsList[3]-1) * tempo;
        return production;
    }
    
}
