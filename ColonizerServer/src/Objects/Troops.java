
package Objects;


import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class Troops extends _Object {
    
    private static int nextID = 1;
    
    private final int truppenArt;
    private final Colony originColony;
    private Colony currentLocation;
    private int quantity;
    
    
    public Troops(int truppenArt, Colony origin, Colony currentLocation, int quantity) {
        super(nextID);
        this.truppenArt = truppenArt;
        this.originColony = origin;
        this.currentLocation = currentLocation;
        this.quantity = quantity;
        nextID++;
    }
    
    
    public Troops(int objectID, int truppenArt, Colony origin, Colony currentLocation, int quantity) {
        super(objectID);
        this.truppenArt = truppenArt;
        this.originColony = origin;
        this.currentLocation = currentLocation;
        this.quantity = quantity;
        nextID = objectID+1;
    }
    
    
    public void addTroop() {
        quantity++;
    }
    
    public Resource getRecrutionCosts(int quantity) {
        Resource costs = new Resource();
        costs.mineral = -troopProperties[truppenArt][0] * quantity;
        costs.treibstoff = -troopProperties[truppenArt][1] * quantity;
        costs.baustoff = -troopProperties[truppenArt][2] * quantity;
        return costs;
    }
    
    public long getTravelTime(Position p) {
        double distance = Position.getDistance(currentLocation.getField().getPosition(), p);
        long travelTime = Math.round(distance * troopProperties[truppenArt][6] * 60 * 1000);
        return travelTime;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public static String getTroopName(int troopTypeNr) {
        return troopList[troopTypeNr];
    }
    
}
