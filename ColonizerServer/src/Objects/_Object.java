
package Objects;


import ServerConstants.*;


/**
 * @author Michael Müller
 */

public abstract class _Object extends ServerConstants {

    protected int objectID;
    
    
    public _Object(int objectID) {
        this.objectID = objectID;
    }
    
    public int getObjectID() {
        return objectID;
    }
    
}
