
package Objects;

import java.util.*;
import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class Colony extends _Object{
        
    private static int nextID = 1;
    private Account owner;
    private Field field;
    private String name;
    private int points;
    private Stock stock;
    private Buildings buildings;
    private Troops[] armee;
    
    private ArrayList<ConstructionTask> constructionList = new ArrayList<ConstructionTask>();
    private ArrayList<RecrutionTask> recrutionList = new ArrayList<RecrutionTask>();
    
    
    /**
     * Constructor
     * @param owner
     * @param field
     * @param name
     * @param stock
     * @param buildings 
     */
    public Colony(Account owner, Field field, String name, Stock stock, Buildings buildings) {
        super(nextID);
        this.owner = owner;
        this.field = field;
        field.setBelegt(true);
        this.name = name;
        this.buildings = buildings;
        this.stock = stock;
        stock.setMaxCapacity(buildings.getMaxStockCapacity());
        armee = new Troops[4];
        nextID++;
        Timer t = new Timer();
        t.schedule(new ResourceProducer(this), 1000, 1000);
    }
    
    /** 
     * Constructor
     * @param colonyID
     * @param owner
     * @param field
     * @param name
     * @param stock
     * @param buildings 
     */
    public Colony(int colonyID, Account owner, Field field, String name, Stock stock, Buildings buildings) {
        super(colonyID);
        this.owner = owner;
        this.field = field;
        field.setBelegt(true);
        this.name = name;
        this.buildings = buildings;
        this.stock = stock;
        stock.setMaxCapacity(buildings.getMaxStockCapacity());
        nextID = colonyID+1;
        Timer t = new Timer();
        t.schedule(new ResourceProducer(this), 1000, 1000);
    }
    
    
    public Account getOwner() {
        return owner;
    }
    
    public Field getField() {
        return field;
    }
    
    public String getName() {
        return name;
    }
    
    public int getPoints() {
        points = 0;
        for (int i = 0; i<buildings.getLevelList().length; i++) {
            points = points + round(Math.pow(buildings.getLevelList()[i], 1.5));
        }
        points += round((stock.getSupply().mineral + stock.getSupply().treibstoff + stock.getSupply().baustoff)/1000);
        return points;
    }
    
    public Stock getStock() {
        return stock;
    }
    
    public Buildings getBuildings() {
        return buildings;
    }
    
    public Troops[] getTroops() {
        return armee;
    }
    
    public ArrayList<ConstructionTask> getConstructionList() {
        return constructionList;
    }
    
    
    //Building Construction
    
    /**
     * Erstellt einen Bauauftrag für das entsprechende Gebäude, falls genug 
     * Rohstoffe vorhanden sind.
     * @param buildingNr
     * @return 
     */
    public boolean addConstructionTask(int buildingNr) {
        boolean succeed = false;
        
        Timer t = new Timer();
        Resource costs;
        long delay;
        Date executionTime;
        ConstructionTask ct;
        
        //Wenn die Liste leer ist...
        if (constructionList.isEmpty()) {
            
            costs = buildings.getUpgradeCosts(buildingNr);
            
            //Wenn die Kosten gedeckt werden können
            if (-costs.mineral < stock.getSupply().mineral && -costs.treibstoff < stock.getSupply().treibstoff 
                    && -costs.baustoff < stock.getSupply().baustoff) {
                delay = buildings.getUpgradeDuration(buildingNr);
                executionTime = new Date(getCurrentTime().getTime() + delay);
                ct = new ConstructionTask(this, buildingNr, delay, executionTime);
                stock.addSupply(costs);
                t.schedule(ct, executionTime);
                constructionList.add(ct);
                
                succeed = true;
            }
            
        //...wenn die Liste nicht leer ist...
        } else {
            
            ConstructionTask lastTask = constructionList.get(constructionList.size()-1);
            
            int level = buildings.getLevelList()[buildingNr];
            for (int i = 0; i<constructionList.size(); i++) {
                if (constructionList.get(i).getBuildingNr() == buildingNr) {
                    level++;
                }
            }
            
            costs = buildings.getUpgradeCosts(buildingNr, level);
            
            //Wenn die Kosten gedeckt werden können
            if (-costs.mineral < stock.getSupply().mineral && -costs.treibstoff < stock.getSupply().treibstoff 
                    && -costs.baustoff < stock.getSupply().baustoff) {
                delay = buildings.getUpgradeDuration(buildingNr, level);
                executionTime = new Date(lastTask.getEstimatedEnd().getTime() + delay);
                ct = new ConstructionTask(this, buildingNr, delay, executionTime);
                stock.addSupply(costs);
                t.schedule(ct, executionTime);
                constructionList.add(ct);
                
                succeed = true;
            }
        }
        
        return succeed;
    }
    
    /**
     * Diese Methode ist für die ConstructionTasks zuständig, die aus der Datenbank
     * importiert werden müssen.
     * @param buildingNr
     * @param executionTime
     */
    public void addConstructionTask(int buildingNr, Date executionTime) {
        Timer t = new Timer();
        
        ConstructionTask ct = new ConstructionTask(this, buildingNr);
        constructionList.add(ct);
        t.schedule(ct, executionTime);
    }
    
    void removeConstructionTask(ConstructionTask ct) {
        constructionList.remove(ct);
    }
    
    /**
     * Liefert die Infos für jeden Bauauftrag in Reihenfolge.
     * 1. GebäudeNr
     * 2. Zielstufe des Gebäudes
     * 3. Startzeit
     * 4. Fertigstellungszeit
     * 5. Verbleibende Dauer
     * @return 
     */
    public String[][] getConstructionListInfo() {
        String[][] list = new String[constructionList.size()][5];
        for (int i = 0; i<constructionList.size(); i++) {
            int buildingNr = constructionList.get(i).getBuildingNr();
            list[i][0] = String.valueOf(buildingNr);
            list[i][1] = String.valueOf(buildings.getLevelList()[buildingNr] + 1);
            list[i][2] = constructionList.get(i).getStartTime().toString();
            list[i][3] = constructionList.get(i).getEstimatedEnd().toString();
            list[i][4] = String.valueOf(constructionList.get(i).getRemainingDuration()/1000); //in Sekunden
        }
        return list;
    }
    
    /**
     * returns the size of constructions
     * @return 
     */
    public int getConstructionListSize(){
        return constructionList.size();
    }
    
    
    //Troop Recrution
    //Diese Features werden in der Erweiterung "BLUTRUNST" vervollständigt.
    
    public boolean addRectrutionTask(int troopTypeNr, int amount) {
        boolean succeed = false;
        
        Timer t = new Timer();
        Resource costs = armee[troopTypeNr].getRecrutionCosts(amount);
        
        if (-costs.mineral < stock.getSupply().mineral && -costs.treibstoff < stock.getSupply().treibstoff
        && -costs.baustoff < stock.getSupply().treibstoff) {
            
            stock.addSupply(costs);
            long delay = Math.round(troopProperties[troopTypeNr][3] * 1000 * Math.pow(buildingProperties[7][4], buildings.getLevelList()[7]-1));
            
            
            
            //Falls die Liste nicht leer ist:
            if (!recrutionList.isEmpty()) {
                
                RecrutionTask lastTask = recrutionList.get(recrutionList.size()-1);
                
                Date start = new Date(lastTask.scheduledExecutionTime());
                long estimatedEnd = start.getTime()+delay*amount;
                
                RecrutionTask rt = new RecrutionTask(this, start, estimatedEnd, amount, troopTypeNr);
                recrutionList.add(rt);
                
                Date firstExecution = new Date(start.getTime()+delay);
                t.schedule(rt, firstExecution, delay);

            //Falls die Liste leer ist:
            } else {
                
                Date start = Calendar.getInstance().getTime();
                long estimatedEnd = start.getTime()+delay*amount;
                
                RecrutionTask rt = new RecrutionTask(this, start, estimatedEnd, amount, troopTypeNr);
                recrutionList.add(rt);
                
                Date firstExecution = new Date(start.getTime()+delay);
                t.schedule(rt, firstExecution, delay);
                
            }
            
            succeed = true;
        }
        
        return succeed;
    }
    
    void removeRecrutionTask(RecrutionTask rt) {
        recrutionList.remove(rt);
    }
    
    /**
     * Liefert die Infos für jeden Rekrutrierungsauftrag in Reihenfolge.
     * 1. Startzeit
     * 2. Fertigstellungszeit
     * 3. Verbleibende Dauer
     * 4. Truppentyp (als Name)
     * 5. Anzahl
     * @return 
     */
    public String[][] getRecrutionTasks() {
        String[][] info = new String[recrutionList.size()][5];
        for (int i = 0; i<recrutionList.size(); i++) {
            info[i][0] = recrutionList.get(i).getStart().toString();
            info[i][1] = recrutionList.get(i).getEstimatedEnd().toString();
            info[i][2] = String.valueOf(recrutionList.get(i).getEstimatedEnd().getTime() - Calendar.getInstance().getTimeInMillis());
            info[i][3] = Troops.getTroopName(recrutionList.get(i).getTroopTypeNr());
            info[i][4] = String.valueOf(recrutionList.get(i).getAnzahl());
        }
        return info;
    }
    public int getRecrutionSize(){
        return recrutionList.size();
    }
    
}



/**
 * Produziert die Rohstoffe.
 * @author Michi
 */
class ResourceProducer extends TimerTask {
    Colony c;
    
    ResourceProducer(Colony c) {
        this.c = c;
    }
    
    @Override
    public void run() {
        c.getStock().addSupply(c.getBuildings().getResourceProducedPerSecond());
    }
    
}

