
package Objects;


import java.util.*;
import java.sql.*;

import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class World extends _Object {
    
    public static int nextID = 1;
    private String worldName;
    private int size;
    private Timestamp startTime;
    private ArrayList<Field> fieldList;
    private double tempo;
    
    /**
     * Constructor.
     * @param worldName
     * @param size
     * @param startTime
     * @param tempo 
     */
    public World(String worldName, int size, Timestamp startTime, double tempo) {
        super(nextID);
        this.worldName = worldName;
        this.size = size;
        this.startTime = startTime;
        fieldList = new ArrayList<Field>();
        this.tempo = tempo;
        createFields();
        nextID++;
    }
    
    /**
     * Construktor.
     * @param worldID
     * @param worldName
     * @param fieldList
     * @param size
     * @param startTime
     * @param tempo 
     */
    public World(int worldID, String worldName, ArrayList<Field> fieldList, int size, Timestamp startTime, double tempo) {
        super(worldID);
        this.worldName = worldName;
        this.fieldList = fieldList;
        this.size = size;
        this.startTime = startTime;
        this.tempo = tempo;
        nextID = worldID+1;
    }
    
    
    
    public String getName() {
        return worldName;
    }
    
    public int getSize() {
        return size;
    }
    
    public Timestamp getStartTime() {
        return startTime;
    }
    
    public double getTempo() {
        return tempo;
    }
    
    public ArrayList<Field> getFieldList() {
        return fieldList;
    }
    
    public Field getFieldWithID(int id) {
        Field f = null;
        for (int i = 0; i<fieldList.size(); i++) {
            if (fieldList.get(i).getObjectID() == id) {
                f = fieldList.get(i);
            }
        }
        return f;
    }
    
    private void createFields() {
        for (int y = 0; y<size; y++) {
            for (int x = 0; x<size; x++) {
                fieldList.add(new Field(new Position(x,y), false));
            }
        }
    }
}
