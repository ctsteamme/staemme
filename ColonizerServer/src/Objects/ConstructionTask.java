
package Objects;


import java.util.*;


/**
 * Bauauftrag zum Upgraden der Gebäude.
 * @author Michael Müller
 */


public class ConstructionTask extends TimerTask {
    
    private Date start, execution;
    
    private final Colony c;
    private final int buildingNr;
    private long duration;
    
    
    /**
     * Constructor.
     * @param c
     * @param buildingNr
     * @param duration 
     * @param execution 
     */
    public ConstructionTask(Colony c, int buildingNr, long duration, Date execution) {        
        this.c = c;
        this.buildingNr = buildingNr;
        this.duration = duration;
        this.execution = execution;
    }
    
    /**
     * Constructor.
     * Für die ConstructionTasks aus der DB...
     * @param c
     * @param buildingNr 
     */
    public ConstructionTask(Colony c, int buildingNr) {
        this.c = c;
        this.buildingNr = buildingNr;
    }
    
    
    @Override
    public void run() {
        c.getBuildings().upgradeBuilding(buildingNr);
        System.out.println("Auftrag " + this + "ausgeführt!");
        c.removeConstructionTask(this);
    }
    
    
    public int getBuildingNr() {
        return buildingNr;
    }
    
    public Date getStartTime() {
        //start.setTime(this.scheduledExecutionTime() - duration);
        return start;
    }
    
    public Date getEstimatedEnd() {
        return execution;
    }
    
    public long getRemainingDuration() {
        return this.scheduledExecutionTime() - Calendar.getInstance().getTimeInMillis();
    }
    
}
