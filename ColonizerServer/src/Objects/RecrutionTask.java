
package Objects;


import java.util.*;


/**
 * Aufträge zum Produzieren von Einheiten.
 * Dieses Feature wird in der Erweiterung "BLUTRUNST" vervollständigt.
 * @author Michael Müller
 */


public class RecrutionTask extends TimerTask {
    
    private final Colony c;
    private final Date start;
    private final long estimatedEnd;
    
    private final int troopTypeNr;
    private int anzahl;
    
    
    public RecrutionTask(Colony c, Date start, long estimatedEnd, int troopTypeNr, int anzahl) {
        this.anzahl = anzahl;
        this.start = start;
        this.estimatedEnd = estimatedEnd;
        this.c = c;
        this.troopTypeNr = troopTypeNr;
    }

    
    @Override
    public void run() {
        if (anzahl > 0) {
            c.getTroops()[troopTypeNr].addTroop();
            anzahl--;
        } else {
            c.removeRecrutionTask(this);
            this.cancel();
        }
    }
    
    public int getTroopTypeNr() {
        return troopTypeNr;
    }
    
    public int getAnzahl() {
        return anzahl;
    }
    
    public Date getStart() {
        return start;
    }
    
    public Date getEstimatedEnd() {
        Date dEstimatedEnd = new Date(estimatedEnd);
        return dEstimatedEnd;
    }
}
