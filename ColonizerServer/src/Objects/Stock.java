
package Objects;


import staemme_shared.*;


/**
 * @author Michael Müller
 */


public class Stock extends _Object{
    
    private static int nextID = 1;
    private int stockID;
    private int maxCapacity;
    private Resource supply;
    
    
    public Stock(Resource supply) {
        super(nextID);
        this.supply = supply;
        nextID++;
    }
    
    public Stock(int stockID, Resource supply) {
        super(stockID);
        this.supply = supply;
        nextID = stockID+1;
    }
    
    
    public static void setNextID(int i) {
        nextID = i;
    }
    
    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }
    
    public void addSupply(Resource r) {
        if (supply.mineral < maxCapacity) {
            supply.mineral += r.mineral;
        }
        if (supply.treibstoff < maxCapacity) {
            supply.treibstoff += r.treibstoff;
        } 
        if (supply.baustoff < maxCapacity) {
            supply.baustoff += r.baustoff;
        }
    }
    
    public Resource getSupply() {
        return supply;
    }
}
