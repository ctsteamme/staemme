
package DB_Server;


import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 * AccountManager.
 * 
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Objekte dieser Klasse sind für die Verbindung zur Datenbank zuständig
 * und führen die ganzen SQL-Abfragen aus.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


/**
 * MAIN DATABASE:
 * IP: staemme.dendacom.net / dendacom.net
 * 
 * USERNAME: d01fe9bb
 * DATABASENAME: d01fe9bb
 * PASSWORD: ynVEnEKYsqX4cW6D
 * 
 * PHPMYADMIN: http://www.dendacom.net/mysqladmin/
 */


public class Verbindung {
    private Connection con = null;
    private String domain, port, dbName, userName, password;
    
    
    public Verbindung(String domain, String port, String dbName, String userName, String password) throws SQLException {
        this.domain = domain;
        this.port = port;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;
    }
    
    public void oeffneVerbindung() throws Exception {
        con = DriverManager.getConnection("jdbc:mysql://" + domain + ":" + port + "/" + dbName + "", "" + userName + "", "" + password + "");
    }
    
    public void trenneVerbindung() throws Exception {
        con.close();
    }
    
    public ResultSet executeQuery(String sqlStatement) throws Exception {
        ResultSet ergebnis = null;
        Statement s = con.createStatement();
        ergebnis = s.executeQuery(sqlStatement);
        return ergebnis;
    }
    
    public void executeUpdate(String sqlStatement) throws Exception {
        PreparedStatement ps = con.prepareStatement(sqlStatement);
        ps.executeUpdate();
    }
    
}
