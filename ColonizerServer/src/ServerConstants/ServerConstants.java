
package ServerConstants;


import java.util.*;
import java.sql.*;


/**
 * Enthält alle wichtigen Konstanten für das Spiel, sowie nützliche globale Methoden.
 * @author Michael Müller
 */
public abstract class ServerConstants {
    
//Globale Abkürzungen
    public static final char tab = '\t';
    
    
//Servereinstellungen
    public static final String DB_USERNAME = "d01fe9bb";
    public static final String DB_PASSWORD = "ynVEnEKYsqX4cW6D";
    public static final String DB_NAME = "d01fe9bb";
    public static final String DB_HOST = "dendacom.net";
    public static final String DB_PORT = "3306";

    
//Gebäudeeinstellungen
    public static final String[] buildingList = {"nexus", "mine", "raffinerie", "aufbereiter", 
        "lagerhalle", "handelsrelais", "labor", "fabrik", "schildgenerator"};    
    
    public static final double ausbaukostenfaktor = 1.234;                      //Faktor für Ausbaukosten je Level
    public static final double ausbauzeitfaktor = 1.33;                         //Faktor für Ausbauzeit je Level
    
    public  static final double produktionsfaktor = 1.23;                       //Faktor für Produktion je Level
    public static final double grundproduktionProSekunde = 0.03125;             //entspricht 112,5 pro Stunde
    
    
    /**
     * Für alle Gebäude gilt:.
     * 0. Grund_Ausbaukosten Mineral
     * 1. Grund_Ausbaukosten Treibstoff
     * 2. Grund_Ausbaukosten Baustoff
     * 3. Grund_Ausbauzeit in Sekunden
     */
    
    /**
     * Einstellungen des Nexus.
     * 4. Faktor für die Ausbauzeit aller Gebäude bei ausgebautem Nexus
     */
    private static final double[] nexus = {60, 40, 20, 100, 0.95};
    
    /**
     * Einstellungen der Produktionsstätten.
     */
    private static final double[] mine = {35, 50, 20, 120};
    private static final double[] raffinerie = {48, 37, 22, 120};
    private static final double[] aufbereiter = {48, 53, 40, 140};
    
    
    
    /**
     * Einstellungen der Lagerhalle.
     * 4. Grund_Maximale_Lagermenge
     * 5. Faktor für maximale Lagermenge
     */
    private static final double[] lagerhalle = {60, 40, 30, 80, 2000, 1.3};
    
    private static final double[] handelsrelais = {70, 40, 40, 60};
    private static final double[] labor = {80, 30, 60, 180};
    
    /**
     * Einstellungen der Fabrik.
     * 4. Faktor für Truppenbauzeiten
     */
    private static final double[] fabrik = {100, 50, 50, 150, 0.95};
    
    /** 
     * Einstellungen des Schildgenerators.
     * 4. Grund_Schildstärke (in Prozent der erhöhten Verteidigungskraft der Truppen)
     * 5. Faktor für die Schildstärke
     */
    private static final double[] schildgenerator = {30, 60, 20, 10, 240, 1.1};
    
    /**
     * Enthält alle oben genannten Eigenschaften.
     */
    public final static double[][] buildingProperties = {nexus, mine, raffinerie, aufbereiter, lagerhalle, handelsrelais, labor, fabrik, schildgenerator};
    
    
//----------------------------------------------------------------------------//
    
//Truppen
    public static final String[] troopList = {"flaggschiff", "korvettenStaffel", 
        "kreuzerStaffel", "massenbeschleuniger"};
    
    /**
     * Für alle Truppen gilt:.
     * 1. Grund_Ausbaukosten Mineral
     * 2. Grund_Ausbaukosten Treibstoff
     * 3. Grund_Ausbaukosten Baustoff
     * 4. Grund_Ausbauzeit in Sekunden
     * 5. Angriffsstärke
     * 6. Verteidigungsstärke
     * 7. Geschwindigkeit (in Minuten pro Streckeneinheit)
     * 8. Tragekapazität
     */
    
    /**
     * Einstellungen des Flaggschiffs.
     * All-Arounder – ist bei jeder Situation passabel.
     * (Gut zum Einstieg ins Geschehen)
     */
    private static final double[] flaggschiff = {30, 60, 40, 500, 50, 50, 40, 100};
    
    /**
     * Einstellungen der Korvettenstaffel.
     * Standard Offensiv-Einheit:
     * Starker Standard-Angriff
     * Schlechte Verteidigung
     */
    private static final double[] korvettenStaffel = {20, 80, 100, 800, 150, 35, 8, 200};
    
    /**
     * Einstellungen der Kreuzerstaffel.
     * Standard Defensiv-Einheit:
     * Schwacher Angriff
     * Starke Verteidigung
     */
    private static final double[] kreuzerStaffel = {100, 50, 50, 600, 10, 150, 30, 50};
    
    /**
     * Einstellungen des Massenbeschleuniger.
     * Einheit zur dauerhaften Zerstörung des Schilds.
     * Kostet viel, bringt aber auch viel...
     */
    private static final double[] massenbeschleuniger = {100, 300, 200, 600, 10, 50, 70, 200};
    
    /**
     * Enthält alle oben genannten Eigenschaften.
     */
    public static final double[][] troopProperties = {flaggschiff, korvettenStaffel, kreuzerStaffel, massenbeschleuniger};
    
//----------------------------------------------------------------------------//
    
    
    /**
     * Liefert die aktuelle Zeit im TimeStamp-Format.
     * @return 
     */
    protected static Timestamp getCurrentTime() {
        Calendar cal = Calendar.getInstance();        
        java.util.Date currentDate = cal.getTime();
        Timestamp ts = new Timestamp(currentDate.getTime());
        return ts;
    }
    
    
    /**
     * Rundet die Zahl auf die nächste ganze Zahl.
     * @param d
     * @return 
     */
    public static int round(double d) {
        return (int) (d+0.5);
    }
}
