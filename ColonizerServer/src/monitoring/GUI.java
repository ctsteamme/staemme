
package monitoring;


import java.util.*;
import java.util.logging.*;

import DB_Server.*;
import Server.*;
import Management.*;

import static ServerConstants.ServerConstants.*;


/**
 * @author David Marchi und Michael Müller
 */


public class GUI extends javax.swing.JFrame {
    
    private ManagementManager manager;
    private boolean verbunden;
    private Timer t;
    
    private Logger log;
    
    
    /**
     * Creates new form GUI
     */
    public GUI() {
        log = Logger.getLogger("GUI - Mainclass");
        
        try {
            manager = new ManagementManager(new Verbindung(DB_HOST, DB_PORT, DB_NAME, DB_USERNAME, DB_PASSWORD));
        } catch(Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        verbunden = false;
        t = new Timer();
        
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnVerbindung = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnInputStartServer = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        btnCreateNewWorld = new javax.swing.JButton();
        txfName = new javax.swing.JTextField();
        txfSize = new javax.swing.JTextField();
        txfTempo = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnVerbindung.setText("Verbinden");
        btnVerbindung.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerbindungActionPerformed(evt);
            }
        });

        jLabel1.setText("1. Datenbank:");

        jLabel2.setText("2. Server starten:");

        btnInputStartServer.setText("Starte Server");
        btnInputStartServer.setEnabled(false);
        btnInputStartServer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInputStartServerActionPerformed(evt);
            }
        });

        btnCreateNewWorld.setText("Erstelle neue Welt");
        btnCreateNewWorld.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateNewWorldActionPerformed(evt);
            }
        });

        jLabel4.setText("Weltname:");

        jLabel5.setText("Weltgröße:");

        jLabel6.setText("Geschwindigkeit:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txfName, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnVerbindung))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnInputStartServer))
                    .addComponent(jSeparator3, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txfSize, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txfTempo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnCreateNewWorld, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnVerbindung))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(btnInputStartServer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txfSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txfTempo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCreateNewWorld)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * will start the connection to the db.
     * @param evt 
     */
    private void btnVerbindungActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerbindungActionPerformed
        try {
            if (verbunden == false) {
                
                manager.connectToDatabase();
                manager.importDatabase();
                                
                verbunden = true;
                t.schedule(manager, 10000, 10000);
                
                btnVerbindung.setText("Trennen");
                btnInputStartServer.setEnabled(true);
            } else {
                
                manager.connectToDatabase();
                manager.updateDatabase();
                manager.disconnectFromDatabase();
                verbunden = false;
                btnVerbindung.setText("Verbinden");
                System.exit(0);
            }
        } catch(Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
        
    }//GEN-LAST:event_btnVerbindungActionPerformed

    /**
     * Starts Server which will then be waiting for incomming Client connections
     * @param evt 
     */
    private void btnInputStartServerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInputStartServerActionPerformed
        try{
            new Thread(new Server(manager)).start();
            btnInputStartServer.setEnabled(false);
            
            log.info("Waiting for Connections...");
       } catch (Exception e){
           log.log(Level.SEVERE, e.getMessage());
       }
    }//GEN-LAST:event_btnInputStartServerActionPerformed

    /**
     * Creates a new World with the given settings.
     * @param evt 
     */
    private void btnCreateNewWorldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateNewWorldActionPerformed
        try {
            String name = txfName.getText();
            int size = Integer.valueOf(txfSize.getText());
            double tempo = Double.valueOf(txfTempo.getText());
            manager.createNewWorld(name, size, tempo);            
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getMessage());
        }
    }//GEN-LAST:event_btnCreateNewWorldActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateNewWorld;
    private javax.swing.JButton btnInputStartServer;
    private javax.swing.JButton btnVerbindung;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTextField txfName;
    private javax.swing.JTextField txfSize;
    private javax.swing.JTextField txfTempo;
    // End of variables declaration//GEN-END:variables
}
