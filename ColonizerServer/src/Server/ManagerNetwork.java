/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import Management.ManagementManager;

/**
 *
 * @author me
 */
public class ManagerNetwork {
    public AccountManagerNetwork amn;
    public ColonyManagementNetwork cmn;

    public ManagerNetwork(ManagementManager mm) {
        
        amn = new AccountManagerNetwork(mm);
        cmn = new ColonyManagementNetwork(mm);
    }
    
    
}
