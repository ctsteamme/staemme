
package Server;

import DB_Server.Verbindung;
import Management.*;
import Objects.Account;
import java.util.ArrayList;
import static staemme_shared.Constants.*;

public class AccountManagerNetwork {

    private ManagementManager mm;

    AccountManagerNetwork(ManagementManager mm) {
        this.mm = mm;
    }
    
    /**
     * 
     * @param msg msg = [TYPE][USERNAME][PASSWORD]
     * @return boolean if account creation was sucessfull
     */
    public String signUp (String msg) {
        
        String[] msgParts = msg.split(SPACE);
        
        String username = msgParts[1];
        String password = msgParts[2];
        
        return ACCOUNT_SIGNUP + SPACE + mm.signUp(username, password);
    }
    
    public String login(String msg){
        System.out.println("LOGIN");
        
        String[] msgParts = msg.split(SPACE);
        System.out.println("" + msgParts[1] + SPACE+ msgParts[2]);
        
        String sessionID = mm.login(msgParts[1], msgParts[2]);
        
        System.out.println(sessionID + "");
        
        return ACCOUNT_SESSIONID + SPACE + sessionID;
    }
}
