
package Server;


import Management.*;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;

import static staemme_shared.Constants.*;
import java.sql.SQLException;
import staemme_shared.Position;
import staemme_shared.Resource;

public class ColonyManagementNetwork {

    private ManagementManager mm;
    
    ColonyManagementNetwork(ManagementManager mm) {
        this.mm = mm;
    }
    
    /**
     * Liefert die allgemeinen (für jeden einsichtbaren) Informationen der Colony.
     * 1. Username
     * 2. Colonyname
     * 3. Points
     * 4. Position (in folgendem Format: xxx|yyy)
     * @param hash
     * @return
     */
    public String getMyColoniesInfo(String msg){
        String[] msgParts = msg.split(SPACE);
        String[] colonyInfos = new String[4];
        //[OwnerName][ColonyName][ColonyPoints][XPOS|YPOS]
        if(Integer.parseInt(msgParts[0]) == COLONY_MANAGEMENT_GET_ColonyInfos){
            if(mm.getMyColoniesInfo(msgParts[1]).length == colonyInfos.length){
                colonyInfos = mm.getMyColoniesInfo(msgParts[1]);
            }else{
                System.err.println("FEHLER getMyColoniesInfo STring[] größe passt nicht");
            }
        }        
        return COLONY_INFOS + SPACE + colonyInfos[0] + SPACE + colonyInfos[1] + SPACE + colonyInfos[2] + SPACE + colonyInfos[3];
    }
    
    public String getMyStockResources(String msg){
        
        String hash = msg.split(SPACE)[1];
        
        int[] stockressource = mm.getMyStockResources(hash);
        System.out.println("" + hash + SPACE + stockressource);
        String resInString = new Resource(stockressource[0], stockressource[1], stockressource[2]).encodeRes();         
        
        
        return resInString;
    }
    
    public String getMyBuildingLevels(String msg){
        String sessionHash = msg.split(SPACE)[1];
        
        int[] buildingLevels = mm.getMyBuildingLevels(sessionHash);
        String levelsAsString = COLONY_BuildingLevel + "";
        for (int i = 0; i < buildingLevels.length; i++) {
            levelsAsString += SPACE;
            levelsAsString += buildingLevels[i];
        }       
        return levelsAsString;
    }
    
    public String getMyConstructionTasks(String msg){
         //public static final String[] buildingList = {"nexus", "mine", "raffinerie", "aufbereiter", 
       // "lagerhalle", "handelsrelais", "labor", "fabrik", "schildgenerator"}; 
        String sessionHash = msg.split(SPACE)[1];
        int constructionListSize = mm.getConstructionSize(sessionHash);
        String[][] constructionTasks = mm.getMyConstructionTasks(sessionHash);
         
        String constructionString = COLONY_CONSTRUCTIONS + SPACE + constructionListSize;
        
        for (int i = 0; i < constructionListSize; i++) {
            for (int j = 0; j < 5; j++) {
                constructionString += SPACE;
                constructionString += constructionTasks[i][4];
            }      
        } 
        return constructionString;
    }

    public String getMyRecrutionTasks(String msg){
        String sessionHash = msg.split(SPACE)[1];
        int recrutionListSize = mm.getRecrutionSize(sessionHash);
        String[][] recrutionTasks = mm.getMyRecrutionTasks(sessionHash);        
        String recrutionString = COLONY_RECRUTIONS + SPACE + recrutionListSize;
        
        for (int i = 0; i < recrutionListSize; i++) {
            for (int j = 0; j < 5; j++) {
                recrutionString += SPACE;
                recrutionString += recrutionTasks[i][j];
            }     
        }        
        return recrutionString;
    }
    
    public String getNearbyColonies(String msg){
        //[Sessionhash]
        String sessionHash = msg.split(SPACE)[1];
        String[][] nearbyColonies = mm.getNearbyColonies(sessionHash);
        String nearbyColoniesTransmission = COLONY_NEARBYCOLONIES + "";
        
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 4; j++) {
                nearbyColoniesTransmission += SPACE;
                nearbyColoniesTransmission += nearbyColonies[i][j];
            }            
        }        
        return nearbyColoniesTransmission;
    }
    
    public String uprageBuilding(String msg){
        //[Sesionhash][buildingNr]
        String sessionID = msg.split(SPACE)[1];
        int bNbr = Integer.parseInt(msg.split(SPACE)[2]);        
        boolean upgradeStatus = mm.upgradeBuilding(sessionID, bNbr);
        return COLONY_OrderStatus + SPACE + upgradeStatus;
    }    
    
    public String recruteTroops(String msg){
        //[SessionHash][troopType][amount]
        String session = msg.split(SPACE)[1];
        int troopType = Integer.parseInt(msg.split(SPACE)[2]);
        int amount = Integer.parseInt(msg.split(SPACE)[3]);        
        return COLONY_OrderStatus + SPACE + mm.recruteTroops(msg, troopType, amount);
    }
    
  
    
}
