
package Server;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import Management.*;
import javax.swing.JTextArea;
import monitoring.GUI;


/**
 * Starts Server, wich will wait for incomming Clients in a seperate Thread.
 * When a new Client connects it will give that socket a extra Thread
 * @author DM
 */
public class Server implements Runnable{
    
    private Logger log;
    private ManagementManager mm;
    private ManagerNetwork mn;
    
    public Server(ManagementManager mm) {
        log = Logger.getLogger("Class_Network");
        this.mm = mm;
        mn = new ManagerNetwork(mm);  

    }  
    
    @Override
    public void run() {
        try {
            //Warte auf Anfragen auf Port 14000:
            ServerSocket serverSocket = new ServerSocket(14000);
            
            while (true) {
                Socket sock = serverSocket.accept();             
                new Thread(new MultiThreadServer(sock, mn)).start();
                log.log(Level.INFO, "Client connected: " + sock.getInetAddress());
            }
        } catch (IOException ex) {
            Logger.getAnonymousLogger().info("ERROR");
            
        }
    }
}



  

