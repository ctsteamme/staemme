

package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Logger;

import Management.ManagementManager;

import static staemme_shared.Constants.*;
/**
 *
 * @author me
 */
public class MultiThreadServer implements Runnable{
    Socket clientSocket;
    Logger log;
    
    ManagerNetwork mn;
    
    MultiThreadServer(Socket clientSocket, ManagerNetwork mn) {
      this.mn = mn;
      this.clientSocket = clientSocket;
      log = Logger.getLogger(MultiThreadServer.class.getName());
    }

    @Override
    public void run() {
        
        try {
            while(clientSocket.isConnected()){
            DataInputStream dIn = new DataInputStream(clientSocket.getInputStream());
                              

        
            String msg = dIn.readUTF();
            System.out.println("INCOMMING: " + msg);
            String answer = "";
            int methodeID = Integer.parseInt(msg.split("::")[0]);
            
            switch(methodeID){
                case ACCOUNT_MANAGEMENT_LOGIN:
                    sendMessage(mn.amn.login(msg));
                    break;                
                case ACCOUNT_MANAGEMENT_SIGNUP:
                    sendMessage(mn.amn.signUp(msg));
                    break;
                case COLONY_MANAGEMENT_GET_ColonyInfos:
                    sendMessage(mn.cmn.getMyColoniesInfo(msg));
                    break;
                case COLONY_MANAGEMENT_GET_Stockressource:
                    sendMessage(mn.cmn.getMyStockResources(msg));
                    break;
                case COLONY_MANAGEMENT_GET_Buildinglevels:
                    sendMessage(mn.cmn.getMyBuildingLevels(msg));
                    break;
                case COLONY_MANAGEMENT_GET_ConstructionTasks:
                    sendMessage(mn.cmn.getMyConstructionTasks(msg));
                    break;
                case COLONY_MANAGEMENT_GET_RecrutionTasks:
                    sendMessage(mn.cmn.getMyRecrutionTasks(msg));
                    break;
                case COLONY_MANAGEMENT_GET_NearbyColonies:
                    sendMessage(mn.cmn.getNearbyColonies(msg));
                    break;
                case COLONY_MANAGEMENT_SET_UpgradeBuilding:
                    sendMessage(mn.cmn.uprageBuilding(msg));
                    break;
                case COLONY_MANAGEMENT_SET_RecruteTroops:
                    sendMessage(mn.cmn.recruteTroops(msg));
                    break;
                default:                    
                    System.out.println("FEHLER CODE NICHT ERKANNT");
            }
            }
        
      }
      catch (IOException e) {
         System.out.println(e);
      } catch (Exception e){
          
      }
    }
    
    public void sendMessage(String msg) throws IOException{
        DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream());
        dOut.writeUTF(msg);
        System.out.println(""+ msg);
        dOut.flush();
    }
}
