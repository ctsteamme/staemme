
package Management;


import java.util.*;
import java.sql.*;

import DB_Server.*;
import Objects.*;
import staemme_shared.*;


/**
 * ColonyManager.
 * 
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Objekte dieser Klasse verwalten die Tabelle 'Colony' sowie 'Stock', 
 * 'BuildingLevels', 'ConstructionQueue' und 'ProductionQueue' innerhalb 
 * der Datenbank sowie die Objekte der entsprechenden Klassen.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


public class ColonyManager extends _Manager {
    
    private ArrayList<Colony> colonyList = new ArrayList<Colony>();
    
    private WorldManager wm;
    private AccountManager am;
    
    
    /**
     * Constructor.
     * @param con
     * @param wm
     * @param am 
     */
    public ColonyManager(Verbindung con, WorldManager wm, AccountManager am) {
        super(con);
        this.wm = wm;
        this.am = am;
    }
    
    
    public ArrayList<Colony> getColonyList() {
        return colonyList;
    }
    

    @Override
    public void importTables() throws Exception {
        colonyList.clear();
        
        ResultSet colonyResult = con.executeQuery("SELECT * FROM Colony");
        while(colonyResult.next()) {
            int colonyID = colonyResult.getInt(1);
            Account acc = am.getObjectWithID(colonyResult.getInt(2));
            Field field = wm.getFieldWithID(colonyResult.getInt(3));
            String name = colonyResult.getString(4);
            int stockID = colonyResult.getInt(6);
            int buildingLevelsID = colonyResult.getInt(7);
            
            Stock stock = null;
            ResultSet stockResults = con.executeQuery("SELECT * FROM Stock WHERE StockID = " + stockID);
            while(stockResults.next()) {
                stock = new Stock(stockID, new Resource(stockResults.getDouble(2), stockResults.getDouble(3), stockResults.getDouble(4)));
            }
            
            Buildings buildings = null;
            ResultSet buildingResults = con.executeQuery("SELECT * FROM BuildingLevels WHERE BuildingLevelsID = " + buildingLevelsID);
            while(buildingResults.next()) {
                int id = buildingResults.getInt(1);
                int[] list = {buildingResults.getInt(2), buildingResults.getInt(3), buildingResults.getInt(4), 
                    buildingResults.getInt(5), buildingResults.getInt(6), buildingResults.getInt(7), 
                    buildingResults.getInt(8), buildingResults.getInt(9), buildingResults.getInt(10)};
                buildings = new Buildings(id, list);
            }
            
            Colony newColony = new Colony(colonyID, acc, field, name, stock, buildings);
            colonyList.add(newColony);
            
            
            ResultSet constructionResult = con.executeQuery("SELECT * FROM RunningConstructions WHERE ColonyID = " + colonyID);
            while(constructionResult.next()) {
                int buildingNr = constructionResult.getInt(3);
                java.util.Date executionTime = constructionResult.getTimestamp(4);
                newColony.addConstructionTask(buildingNr, executionTime);
            }
            
            
            /**
             * Dieses Feature wird in der Erweiterung "BLUTRUNST" vervollständigt.
             */
//            ResultSet productionResult = con.executeQuery("SELECT * FROM RunningProductions WHERE ColonyID = " + colonyID);
//            while (productionResult.next()) {
//                ...
//                ...
//                ...
//            }
        }
    }
    
    @Override
    protected void updateTables() throws Exception {
        con.executeUpdate("TRUNCATE Stock");
        con.executeUpdate("TRUNCATE BuildingLevels");
        con.executeUpdate("TRUNCATE RunningConstructions");
        //con.executeUpdate("TRUNCATE RunningRecrutions");
        con.executeUpdate("TRUNCATE Colony");
        
        
        for (int i = 0; i<colonyList.size(); i++) {
            
            //Stock-Tabelle updaten...
            int stockID = colonyList.get(i).getStock().getObjectID();
            int mineral = round(colonyList.get(i).getStock().getSupply().mineral);
            int treibstoff = round(colonyList.get(i).getStock().getSupply().treibstoff);
            int baustoff = round(colonyList.get(i).getStock().getSupply().baustoff);
            con.executeUpdate("INSERT INTO Stock (StockID, Mineral, Treibstoff, Baustoff) "
                            + "VALUES ('" + stockID + "', '" + mineral + "', '" + treibstoff + "', '" + baustoff + "');");
            
            //BuildingLevels-Tabelle updaten...
            int buildingsID = colonyList.get(i).getBuildings().getObjectID();
            int[] levels = colonyList.get(i).getBuildings().getLevelList();
            con.executeUpdate("INSERT INTO BuildingLevels (BuildingLevelsID, Nexus, "
                            + "Mine, Raffinerie, Aufbereiter, Lagerhalle, "
                            + "Handelsrelais, Labor, Fabrik, Schildgenerator) "
                            + "VALUES ('" + buildingsID + "', '" + levels[0] + "', '" 
                            + levels[1] + "', '" + levels[2] + "', '" + levels[3] + "', '" 
                            + levels[4] + "', '" + levels[5] + "', '" + levels[6] + "', '" 
                            + levels[7] + "', '" + levels[8] + "');");
            
            //Colony-Tabelle updaten
            int colonyID = colonyList.get(i).getObjectID();            
            int ownerID = colonyList.get(i).getOwner().getObjectID();
            int fieldID = colonyList.get(i).getField().getObjectID();
            String name = colonyList.get(i).getName();
            int points = colonyList.get(i).getPoints();
            con.executeUpdate("INSERT INTO Colony (ColonyID, OwnerID, FieldID, name, points, StockID, BuildingLevelsID) "
                            + "VALUES ('" + colonyID + "', '" + ownerID + "', '" + fieldID + "', '" 
                            + name + "', '" + points + "', '" + stockID + "', '" + buildingsID + "');");
            
            //RunningConstructions-Tabelle updaten...
            if (!colonyList.get(i).getConstructionList().isEmpty()) {
                for (int j = 0; j<colonyList.get(i).getConstructionList().size(); j++) {
                    int buildingNr = colonyList.get(i).getConstructionList().get(j).getBuildingNr();
                    Timestamp executionTime = new Timestamp(colonyList.get(i).getConstructionList().get(j).getEstimatedEnd().getTime());
                    con.executeUpdate("INSERT INTO RunningConstructions (ColonyID, BuildingNr, executionTime) "
                                    + "VALUES ('" + colonyID + "', '" + buildingNr + "', '" + executionTime + "');");
                }
            }
            
            
            //RunningRecrutions-Tabelle updaten...
            //Kommt in der nächsten Erweiterung "BLUTRUNST"
        }
    }
    
    @Override
    public Colony getObjectWithID(int id) {
        Colony c = null;
        for (int i = 0; i<colonyList.size(); i++) {
            if (i == id) {
                c = colonyList.get(i);
            }
        }
        return c;
    }
    
    @Override
    public void deleteObject(int id) {
        colonyList.remove(getObjectWithID(id));
    }
    
    /**
     * Erstellt auf der Standard-Welt eine Colony mit dem gewünschten Namen für 
     * entsprechenden Account.
     * @param account
     * @param colonyName 
     */
    public void createColony(Account account, String colonyName) {
        World world = wm.getWorldList().get(0);
        
        //Suche alle unbelegten Felder und wähle ein zufälliges Feld aus
        ArrayList<Field> possibleFields = new ArrayList<Field>();
        for (int i = 0; i<world.getFieldList().size(); i++) {
            if (!world.getFieldList().get(i).getBelegt()) {
                possibleFields.add(world.getFieldList().get(i));
            }
        }
        Field f = possibleFields.get((int) (possibleFields.size() * Math.random()));
        
        //Erstelle Stock und BuildingLevels
        Stock s = new Stock(new Resource(500, 500, 500));
        Buildings bl = new Buildings(world.getTempo());

        colonyList.add(new Colony(account, f, colonyName, s, bl));
        
    }
    
    /**
     * Erstellt für den gewünschten Account auf der gewünschten Welt eine neue 
     * Colony auf einem zufälligen Feld mit den Standardwerten für Stock und 
     * Gebäudestufen (sprich, BuildingLevels)
     * @param world
     * @param account
     * @param colonyName 
     */
    public void createColony(World world, Account account, String colonyName) {
        
        //Suche alle unbelegten Felder und wähle ein zufälliges Feld aus
        ArrayList<Field> possibleFields = new ArrayList<Field>();
        for (int i = 0; i<world.getFieldList().size(); i++) {
            if (!world.getFieldList().get(i).getBelegt()) {
                possibleFields.add(world.getFieldList().get(i));
            }
        }
        Field f = possibleFields.get((int) (possibleFields.size() * Math.random()));
        
        //Erstelle Stock und Buildings
        Stock s = new Stock(new Resource(500, 500, 500));
        Buildings bl = new Buildings(world.getTempo());

        colonyList.add(new Colony(account, f, colonyName, s, bl));
    }

    
    private Colony getColonyWithHash(String hash) {
        Colony c = null;
        for (int i = 0; i<colonyList.size(); i++) {
            if (colonyList.get(i).getOwner().getSessionHash().equals(hash)) {
                c = colonyList.get(i);
            }
        }
        return c;
    }
    
    
    //Infos
    
    /**
     * Liefert die allgemeinen (für jeden einsichtbaren) Informationen der Colony.
     * @param c
     * @return 
     */
    private String[] getGeneralInfo(Colony c) {
        String[] s = {c.getOwner().getName(),
                      c.getName(), 
                      String.valueOf(c.getPoints()), 
                      c.getField().getPosition().x + "|" + c.getField().getPosition().y};
        return s;
    }
    
    
    /**
     * Liefert die allgemeinen (für jeden einsichtbaren) Informationen der Colony.
     * 1. Username
     * 2. Colonyname
     * 3. Points
     * 4. Position (in folgendem Format: xxx|yyy)
     * @param hash
     * @return
     */
    public String[] getGeneralInfoWithHash(String hash) {
        Colony c = getColonyWithHash(hash);
        return getGeneralInfo(c);
    }
    
    /**
     * Liefert die zurzeit verfügbaren Ressourcen.
     * @param hash
     * @return 
     */
    public int[] getStockResources(String hash) {
        Colony c = getColonyWithHash(hash);
        int[] resources = {round(c.getStock().getSupply().mineral),
                           round(c.getStock().getSupply().treibstoff),
                           round(c.getStock().getSupply().baustoff)};
        return resources;
    }
    
    /**
     * Liefert die Gebäudestufen.
     * @param hash
     * @return 
     */
    public int[] getBuildingLevelList(String hash) {
        Colony c = getColonyWithHash(hash);
        return c.getBuildings().getLevelList();
    }
        
    /**
     * Liefert die generellen Infos der 10 nächsten Colonies.
     * @param hash
     * @return 
     */
    public String[][] getNearbyColonies(String hash) {
        Position myPos = getColonyWithHash(hash).getField().getPosition();
        double[] distanceList = new double[colonyList.size()-1];
        
        int j = 0;      //Extra Variable, da j einmal weniger vorkommen darf/muss als i.
        for (int i = 0; i<colonyList.size(); i++) {
            if (colonyList.get(i) != getColonyWithHash(hash)) {
                distanceList[j] = Position.getDistance(myPos, colonyList.get(i).getField().getPosition());
                j++;    //Kommt einmal weniger vor (wegen if-Abfrage)
            }
        }
        Arrays.sort(distanceList);
        
        Colony[] nearbyColonies = new Colony[10];
        for (int i = 0; i<10; i++) {
            for (int k = 0; k<colonyList.size(); k++) {
                if (Position.getDistance(myPos, colonyList.get(k).getField().getPosition()) == distanceList[i]) {
                    nearbyColonies[i] = colonyList.get(k);
                }
            }
        }
        
        String info[][] = new String[10][4];
        for (int i = 0; i<10; i++) {
            info[i] = getGeneralInfo(nearbyColonies[i]);
        }
        return info;
    }
    
    
    //Construction Tasks
    
    /**
     * Erstellt einen neuen Bauauftrag
     * @param hash
     * @param buildingNr
     * @return 
     */
    public boolean newConstruction(String hash, int buildingNr) {
        Colony c = getColonyWithHash(hash);
        return c.addConstructionTask(buildingNr);
    }

    /**
     * Liefert die Infos der aktuell laufenden Konstruktionen.
     * @param hash
     * @return 
     */
    public String[][] getConstructionTasks(String hash) {
        Colony c = getColonyWithHash(hash);
        return c.getConstructionListInfo();
    }
    
    /**
     * Get Size
     * @param hash
     * @return 
     */
    
    public int getConstructionSize(String hash){
        Colony c = getColonyWithHash(hash);
        return c.getConstructionListSize();
    }
    
    //Recrution Tasks
    
    /**
     * Rekrutiert die entsprechenden Truppen.
     * @param hash
     * @param troopTypeNr
     * @param amount
     * @return 
     */
    public boolean newRecrution(String hash, int troopTypeNr, int amount) {
        Colony c = getColonyWithHash(hash);
        return c.addRectrutionTask(troopTypeNr, amount);
    }
    
    /**
     * Liefert die Infos der aktuell laufenden Rekrutierungen.
     * @param hash
     * @return 
     */
    public String[][] getRecrutionTasks(String hash) {
        Colony c = getColonyWithHash(hash);
        return c.getRecrutionTasks();
    }
    public int getRecrutionSize(String hash){
        return getColonyWithHash(hash).getConstructionListSize();
    }
}
