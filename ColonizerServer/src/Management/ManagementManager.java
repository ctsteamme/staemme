
package Management;


import java.util.*;

import DB_Server.*;


/**
 * ManagementManager.
 * 
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Objekte dieser Klasse verbinden Account-, World-, und Colonymanager zu einem
 * einfach bedienbaren Manager, von der aus die Clients auf alle ihre Methoden
 * an einem gesammelten Ort zugreifen können und auch das Serverprogramm alle
 * benötigten Methoden hier findet.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


public class ManagementManager extends TimerTask {
    
    private static Verbindung con;
    public static AccountManager am;
    public static WorldManager wm;
    public static ColonyManager cm;
        
    /**
     * Konstruktor
     * @param con 
     */
    public ManagementManager(Verbindung con) {
        this.con = con;
        this.am = new AccountManager(con);
        this.wm = new WorldManager(con);
        this.cm = new ColonyManager(con, wm, am);
    }
    
//----------------------------------------------------------------------------//
    
    /**
     * Serverseitige Methoden.
     * Folgende Methoden werden nur vom Server aufgerufen, nicht von den Clients...
     */
    
    /**
     * Stellt eine Verbindung zur Datenbank her.
     * @throws Exception 
     */
    public void connectToDatabase() throws Exception {
        con.oeffneVerbindung();
    }
    
    /**
     * Trennt die Verbindung zur Datenbank.
     * @throws Exception 
     */
    public void disconnectFromDatabase() throws Exception {
        con.trenneVerbindung();
    }
    
    /**
     * Lädt die Datenbank ins Programm.
     * @throws Exception 
     */
    public void importDatabase() throws Exception {
        am.importTables();
        wm.importTables();
        cm.importTables();
    }
    
    /**
     * Schreibt die geänderten Daten in die Datenbank
     * @throws Exception 
     */
    public void updateDatabase() throws Exception {
        am.updateTables();
        wm.updateTables();
        cm.updateTables();
    }
    
    /**
     * Erstellt eine neue Welt.
     * @param name
     * @param size
     * @param tempo
     */
    public void createNewWorld(String name, int size, double tempo){
        wm.createNewWorld(name, size, tempo);
    }
    
//----------------------------------------------------------------------------//    
    
    /**
     * Clientseitige Methoden.
     * Folgende Methoden werden nur von den Clients aufgerufen, nicht vom Server...
     */
    
    /**
     * User registriert sich.
     * @param name
     * @param pwHash
     * @return 
     */
    public synchronized  boolean signUp(String name, String pwHash) {
        if(am.signUp(name, pwHash)) {
            System.out.println("1234567");
            cm.createColony(am.getAccountWithPwHash(pwHash), "Dorf 123");
            return true;
        } else return false;
    }
    
    /**
     * User loggt sich ein.
     * @param username
     * @param passwordHash
     * @return sessionID
     */
    public synchronized String login(String username, String passwordHash) { 
        return am.login(username, passwordHash);
    }
    
    /**
     * Spieler erstellt auf der Standard-Welt (Welt 1) seine Colony.
     * @param sessionHash
     * @param colonyName 
     */
    public synchronized void enterWorld(String sessionHash, String colonyName) {
        cm.createColony(am.getAccountWithSessionHash(sessionHash), colonyName);
    }
    
    /**
     * Spieler erstellt auf einer bestimmten Welt seine Colony.
     * @param worldNr
     * @param sessionHash
     * @param colonyName 
     */
    public synchronized void enterWorld(int worldNr, String sessionHash, String colonyName) {
        cm.createColony(wm.getObjectWithID(worldNr), am.getAccountWithSessionHash(sessionHash), colonyName);
    }
    
    
    //Eigene Infos
    
    /**
     * Liefert die allgemeinen Informationen des eigenen Dorfs.
     * @param hash
     * @return 
     */
    public synchronized String[] getMyColoniesInfo(String hash) {
        return cm.getGeneralInfoWithHash(hash);
    }
    
    /**
     * Liefert die eigenen verfügbaren Ressourcen.
     * @param hash
     * @return 
     */
    public synchronized int[] getMyStockResources(String hash) {
        return cm.getStockResources(hash);
    }
    
    /**
     * Liefert die eigenen Gebäudestufen.
     * @param hash
     * @return 
     */
    public synchronized int[] getMyBuildingLevels(String hash) {
        return cm.getBuildingLevelList(hash);
    }
    
    /**
     * Liefert die eigenen Bauaufträge.
     * @param hash
     * @return 
     */
    public synchronized String[][] getMyConstructionTasks(String hash) {
        return cm.getConstructionTasks(hash);
    }
    public synchronized int getConstructionSize(String hash){
        return cm.getConstructionSize(hash);
    }
    
    
    public synchronized String[][] getMyRecrutionTasks(String hash) {
        return cm.getRecrutionTasks(hash);
    }
    public synchronized int getRecrutionSize(String hash){
        return cm.getConstructionSize(hash);
    }
    
    /**
     * Liefert die 10 nächsten Colonies.
     * @param hash
     * @return 
     */
    public synchronized String[][] getNearbyColonies(String hash) {
        return cm.getNearbyColonies(hash);
    }
    
    
    //Aufträge
    
    /**
     * Baut das gewünschte Gebäude aus.
     * Verbuggt!
     * @param hash
     * @param buildingNr
     * @return 
     */
    public synchronized boolean upgradeBuilding(String hash, int buildingNr) {
        return cm.newConstruction(hash, buildingNr);
    }
    
    /**
     * Rekrutiert die entsprechenden Truppen.
     * Nicht funktionsfähig!!
     * @param hash
     * @param troopTypeNr
     * @param amount
     * @return 
     */
    public synchronized boolean recruteTroops(String hash, int troopTypeNr, int amount) {
        return cm.newRecrution(hash, troopTypeNr, amount);
    }
    
    public synchronized int[] trade(String sessionID, String type, double amount){
        /**
         * .....
         */
        return getMyStockResources(sessionID);
    }

    @Override
    public void run() {
        try {
            connectToDatabase();
            updateDatabase();
        } catch (Exception e) {
            System.out.println("Fehler beim Updaten der Datenbank!");
        }
        
        
        
        
    }
    
    
}
