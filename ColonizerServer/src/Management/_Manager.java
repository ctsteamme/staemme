
package Management;


import ServerConstants.ServerConstants;
import Objects._Object;
import java.util.*;
import java.sql.*;

import DB_Server.*;


/**
 * _Manager.
 * 
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Diese abstrakte Klasse abstrahiert alle abstahierbaren Methoden der 
 * verschiedenen Manager-Klassen.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


abstract public class _Manager extends ServerConstants {
    
    protected Verbindung con;
    
    
    protected _Manager(Verbindung connection) {
        con = connection;
    }
    
    
    /**
     * Importiert die jeweilige(n) Tabelle(n) der Datenbank in das Programm.
     * @throws Exception 
     */
    protected abstract void importTables() throws Exception;
    
    /**
     * Überschreibt die jeweilige(n) Tabelle(n) der Datenbank mit den neuen Werten.
     * @throws Exception 
     */
    protected abstract void updateTables() throws Exception;
    
    /**
     * Liefert das Object mit der entsprechenden ID.
     * @param id
     * @return 
     */
    protected abstract _Object getObjectWithID(int id);
    
    /**
     * Löscht das Object mit der entsprechenden ID.
     * @param id 
     */
    protected abstract void deleteObject(int id);
    
}
