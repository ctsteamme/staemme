
package Management;


import Objects.*;
import java.util.*;
import java.sql.*;
import DB_Server.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import staemme_shared.*;


/**
 * AccountManager.
 * 
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Objekte dieser Klasse verwalten die Tabellen 'Account' innerhalb 
 * der Datenbank sowie die Objekte der Klasse 'Account'.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


public class AccountManager extends _Manager {

  
    
    private ArrayList<Account> accountList = new ArrayList<Account>();
    
    
    public AccountManager(Verbindung con) {
        super(con);
    }
    
    
    public ArrayList getAccountList() {
        return accountList;
    }
    

    @Override
    public void importTables() throws Exception {
        accountList.clear();
        
        ResultSet rs = con.executeQuery("SELECT * FROM Account");
        while(rs.next()) {
            accountList.add(new Account(rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
    }
    
    @Override
    public void updateTables() throws Exception {
        con.executeUpdate("TRUNCATE Account");

        for (int i = 0; i<accountList.size(); i++) {
            int id = accountList.get(i).getObjectID();
            String name = accountList.get(i).getName();
            String pw = accountList.get(i).getPwHash();
            con.executeUpdate("INSERT INTO Account(AccountID, AccountName, PasswordHash)"
                            + "VALUES('" + id + "', '" + name + "', '" + pw + "')");
        }
    }
    
    @Override
    public Account getObjectWithID(int id) {
        Account a = null;
        for (int i = 0; i<accountList.size(); i++) {
            if (accountList.get(i).getObjectID() == id) {
                a = accountList.get(i);
            }
        }
        return a;
    }
    
    public Account getAccountWithSessionHash(String sessionHash) {
        Account a = null;
        for (int i = 0; i<accountList.size(); i++) {
            if (accountList.get(i).getSessionHash().equals(sessionHash)) {
                a = accountList.get(i);
            }
        }
        return a;
    }
    
    public Account getAccountWithPwHash(String pwHash) {
        Account a = null;
        for (int i = 0; i<accountList.size(); i++) {
            if (accountList.get(i).getPwHash().equals(pwHash)) {
                a = accountList.get(i);
            }
        }
        return a;
    }
    
    @Override
    public void deleteObject(int id) {
        accountList.remove(getObjectWithID(id));
    }

    
     /**
     * Creates a new Account.
     * @param name
     * @param passwortHash 
     */
    public void createNewAccount(String name, String passwortHash) {
        accountList.add(new Account(name, passwortHash));
    }
    
    /**
     * Erstellt einen neuen Account, falls der Name nicht schon vergeben ist.
     * @param name
     * @param pwHash
     * @return 
     */
    public boolean signUp(String name, String pwHash) {
        boolean succeed = true;
        for (int i = 0; i<accountList.size(); i++) {
            if (accountList.get(i).getName().equals(name)) {
                succeed = false;
            }
        }
        if (succeed == true) {
            createNewAccount(name, pwHash);
        }
        return succeed;
    }
    
    /**
     * User loggt sich ein.
     * Sollte hier nicht dann in der SESSION Tabelle der USER mit dem HASH verbunden werden, sodass man dann immer weis, welcher HASH zu welchem User gehört?
     * Was kann man tun, damit zu 100% Sicherheit kein Hash doppelt vorkommt?
     * @param username
     * @param hash
     * @return 
     */
    public String login(String username, String hash) {
        String sessionHash = null;
        Account a = null;
        for (int i = 0; i<accountList.size(); i++) {
            if (accountList.get(i).getName().equals(username)) {
                if (accountList.get(i).getPwHash().equals(hash)) {
                    a = accountList.get(i);
                    sessionHash = generateHash();
                    a.setSessionHash(sessionHash);
                }
            }
        }
        return sessionHash;
    }
    
    //Vielleicht noch irgendwas mit der aktuellen Zeit?  Nur um sicher zu gehen, dass nichts doppelt vorkommt.
    //Ich denke es würde auch reichen, wenn du die Zufallszahl + aktuelle Zeit mit MD5 oder SHA256 hashst, dann hat man immer die selbe Form 
    //und muss sich nicht mit Sonderzeichen auf die gefährliche Fährte begeben.
    private String generateHash() {
        
        
        
        String hash = "123";
        hash += System.currentTimeMillis() + "";
        hash += Math.random();
        
        hash = md5Java(hash);

//        String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
//                            + "0123456789=!§$%&/()+*#-_´`.:,;/";
//        Random r = new Random();
//        for (int i = 0; i<r.nextInt(32); i++) {
//            hash += allowedChars.charAt(r.nextInt(allowedChars.length()));
//        }
        
        return hash;
    }
    
    private String md5Java(String message){
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8"));

            //converting byte array to Hexadecimal String
            StringBuilder sb = new StringBuilder(2*hash.length);
            for(byte b : hash){
                sb.append(String.format("%02x", b&0xff));
            }

            digest = sb.toString();

        } catch (Exception ex) {
        
        }
        return digest;
    }
    
}
