
package Management;


import java.util.*;
import java.sql.*;

import DB_Server.*;
import Objects.*;
import staemme_shared.*;


/**
 * WorldManager.
 *
 * 
 * ------------------------------!!READ ME!!---------------------------------- * 
 * Objekte dieser Klasse verwalten die Tabellen 'World' und 'Field' innerhalb
 * der Datenbank sowie die Objekte der Klassen 'World' und 'Field'.
 * ------------------------------!!READ ME!!---------------------------------- *
 * 
 * 
 * @author Michael Müller
 */


public class WorldManager extends _Manager {    
    
    private ArrayList<World> worldList = new ArrayList<World>();
    
    
    /**
     * Constructor
     * @param con 
     */
    public WorldManager(Verbindung con) {
        super(con);
    }
    
    
    public ArrayList<World> getWorldList() {
        return worldList;
    }
    
    
    @Override
    public void importTables() throws Exception  {
        worldList.clear();
        
        ResultSet rs = con.executeQuery("SELECT * FROM World");
        while(rs.next()) {
            int worldID = rs.getInt(1);
            ResultSet fieldResult = con.executeQuery("SELECT * FROM Field WHERE WorldID = " + worldID);
            ArrayList<Field> fieldList = new ArrayList<Field>();
            while (fieldResult.next()) {
                fieldList.add(new Field(fieldResult.getInt(1), 
                              new Position(fieldResult.getInt(3), fieldResult.getInt(4)),
                              fieldResult.getBoolean(5)));
            }
            worldList.add(new World(rs.getInt(1), rs.getString(2), fieldList, rs.getInt(3), rs.getTimestamp(4), rs.getDouble(5)));
        }
    }
    
    @Override
    public void updateTables() throws Exception {
        con.executeUpdate("TRUNCATE Field");
        con.executeUpdate("TRUNCATE World");
        
        for (int i = 0; i<worldList.size(); i++) {
            int worldID = worldList.get(i).getObjectID();
            String name = worldList.get(i).getName();
            int size = worldList.get(i).getSize();
            Timestamp currentTime = worldList.get(i).getStartTime();
            double tempo = worldList.get(i).getTempo();
            
            con.executeUpdate("INSERT INTO World(WorldID, WorldName, Size, StartTime, Tempo)"
                            + "VALUES ('" + worldID + "', '" + name + "', '" + size + "', '" + currentTime + "', '" + tempo + "');");
            
            
            for (int j = 0; j<worldList.get(i).getFieldList().size(); j++) {
                int fieldID = worldList.get(i).getFieldList().get(j).getObjectID();
                int xCoord = worldList.get(i).getFieldList().get(j).getPosition().x;
                int yCoord = worldList.get(i).getFieldList().get(j).getPosition().y;
                boolean b = worldList.get(i).getFieldList().get(j).getBelegt();
                
                
                con.executeUpdate("INSERT INTO Field(FieldID, WorldID, xCoord, yCoord, belegt)"
                                + "VALUES ('" + fieldID + "', '" + worldID + "', '" + xCoord + "', '" + yCoord + "', " + b + ");");
                
                
                
            }
        }
    }
    
    @Override
    public World getObjectWithID(int id) {
        World w = null;
        for (int i = 0; i<worldList.size(); i++) {
            if (worldList.get(i).getObjectID() == id) {
                w = worldList.get(i);
            }
        }
        return w;
    }
    
    @Override
    public void deleteObject(int worldID) {
        worldList.remove(getObjectWithID(worldID));
    }
    
    /**
     * Erstellt eine neue Welt mit den gegebenen Parametern 'Weltname', Größe der
     * Welt und dem Spieltempo, sowie alle dazugehörigen Felder
     * @param name
     * @param size
     * @param tempo 
     */
    public void createNewWorld(String name, int size, double tempo) {
        worldList.add(new World(name, size, getCurrentTime(), tempo));
    }
    
    public Field getFieldWithID(int id) {
        Field f = null;
        for (int i = 0; i<worldList.size(); i++) {
            f = worldList.get(i).getFieldWithID(id);
        }
        return f;
    }
    
    public Field getFieldWithID(World w, int id) {
        Field f = w.getFieldWithID(id);
        return f;
    }
    
}
