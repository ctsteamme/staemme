/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package staemme_shared;

import static staemme_shared.Constants.*;

/**
 * Resource Class.
 * Easy way to access and store the ressources
 * @author DM
 */
public class Resource {
    public double mineral, treibstoff, baustoff;

    /**
     * Resource constructor.
     * 
     * just pass them in and go for it.
     * 
     * 
     * @param mineral
     * @param treibstoff
     * @param baustoff 
     */
    public Resource(double mineral, double treibstoff, double baustoff) {
        this.mineral = mineral;
        this.treibstoff = treibstoff;
        this.baustoff = baustoff;          
    }
    
    public Resource(){
        
    }
    
    
    public String encodeRes(){
        String encodedPos = "";
        encodedPos = TYPE_RES + SPACE + mineral + SPACE + treibstoff + SPACE + baustoff;
        
        return encodedPos;
    }
    
    public Resource decodeRes(String encodedRes) throws Exception{
        
        String encodedParts[] = encodedRes.split(SPACE);
        if(Integer.parseInt(encodedParts[0]) != TYPE_RES){
            throw new Exception("Typ nicht erkannt!");
        }else{
           mineral = Double.parseDouble(encodedParts[1]);
           treibstoff = Double.parseDouble(encodedParts[2]);
           baustoff = Double.parseDouble(encodedParts[3]);
        }
      
        
        return this;
    }
    
    
}
