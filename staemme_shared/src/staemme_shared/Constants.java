package staemme_shared;





/**
 * StaticConstant Class.
 * 
 * Constanten will be needed for the Network transmission, nvm!
 * Should be equal on Server & Client!!!
 * 
 * @author DM
 */
public class Constants {
    //METHODEN
    public static final int COLONY_MANAGEMENT_GET_ColonyInfos = 20009;
    public static final int COLONY_MANAGEMENT_GET_Stockressource = 20008;
    public static final int COLONY_MANAGEMENT_GET_Buildinglevels = 20010;
    public static final int COLONY_MANAGEMENT_GET_ConstructionTasks = 20011;
    public static final int COLONY_MANAGEMENT_GET_RecrutionTasks = 20012;
    public static final int COLONY_MANAGEMENT_GET_NearbyColonies = 20013;
    
    public static final int COLONY_MANAGEMENT_SET_UpgradeBuilding = 21001;
    public static final int COLONY_MANAGEMENT_SET_RecruteTroops = 21002;
    

    //TYPE::USERNAME::HASH
    public static final int ACCOUNT_MANAGEMENT_LOGIN = 40001;
    public static final int ACCOUNT_MANAGEMENT_SIGNUP = 40002;
    
    
    
    
    
    //VariablenTypen
    public static final int ACCOUNT_SESSIONID = 41001;
    public static final int ACCOUNT_USERNAME = 41002;
    public static final int ACCOUNT_PASSWORD = 41003;
    public static final int ACCOUNT_SIGNUP = 41004;

    public static final int COLONY_INFOS = 42001;
    public static final int COLONY_BuildingLevel = 42002;
    public static final int COLONY_CONSTRUCTIONS = 42003;
    public static final int COLONY_RECRUTIONS = 42004;
    public static final int COLONY_NEARBYCOLONIES = 42005;
    
    public static final int COLONY_OrderStatus = 42006;
    
    public static final int TYPE_POS = 70001;
    public static final int TYPE_RES = 70002;
    
    public static final String SPACE = "::";

}

