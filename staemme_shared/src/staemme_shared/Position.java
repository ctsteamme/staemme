
package staemme_shared;
import static staemme_shared.Constants.*;
import java.lang.Exception;


/**
 * Position Class.
 * 
 * Easy way to store XY coordinates
 * 
 * @author DM
 * @param x is Public
 * @param Y is Public
 */
public class Position {
    public int x;
    public int y;
    
    /**
     * PositionConstructor.
     * 
     * just passin the x & y
     * Then you can access them through the attributes
     * pos.y or pos.x
     * 
     * @param x 
     * @param y 
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }  
    
    public Position(){
        
    }
    
    public String encodePos(){
        String encodedPos = "";
        encodedPos += TYPE_POS + SPACE + x + SPACE + y;
        
        return encodedPos;
    }
    
    public Position decodePos(String encodedPos) throws Exception{
        String encodedParts[] = encodedPos.split(SPACE);
        if(Integer.parseInt(encodedParts[0]) != TYPE_POS){
            throw new Exception("Typ nicht erkannt!");
        }else{
           x = Integer.parseInt(encodedParts[1]);
           y = Integer.parseInt(encodedParts[2]);
        }
      
        
        return this;
    }
    
    public static double getDistance(Position p1, Position p2) {
        int x1 = p1.x; int x2 = p2.x;
        int y1 = p1.y; int y2 = p2.y;
        double distance = Math.pow(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2), 0.5);
        return distance;
    }
    
}
