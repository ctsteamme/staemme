package frontend;

import backend.networking.AccountManager;
import javax.swing.JDialog;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import staemme_shared.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import backend.networking.ColonyManager;
import backend.networking.ServerConnection;
import java.io.IOException;



public class MainWindow extends javax.swing.JFrame {
    
    private String session;
    // Anzahl möglicher Buttons, Anzahl möglicher Paramater & Arrays für die Buttons
    private int num_buttons = 10;
    private int num_parms   = 5;
    private int buttons[][] = new int[num_parms][num_buttons];
    private String actions[][] = new String[2][num_buttons];
    
    
    

    private Graphics g;
    private Graphics2D g2 = (Graphics2D) g;
    
    
    // Hintergrundfarben für Textfelder
    private Color grey_topbar  = new Color(74,72,72);
    private Color grey_sidebar = new Color(82,81,81);
    
    // Pfad zum Hintergrundbild
    private Image img1 = Toolkit.getDefaultToolkit().getImage("../assets/images/kolonie/UI-Final.png");
    
    //ATTRIBUTE DER GUI
    private Resource graphicStockRessources;
    private String graphicBuildingInfo;
    private String graphicBuildingName;
    private String graphicBuildingLevel;
    
    private ServerConnection sc;
    private AccountManager am;
    private ColonyManager cm;
    
    public MainWindow(String session, ServerConnection sc) throws Exception {
        this.sc = sc;
        
        this.session = session;
        
        am = new AccountManager(sc);
        cm = new ColonyManager(sc);
        
        graphicBuildingName  = "";
        graphicBuildingInfo  = "";
        graphicBuildingLevel = "";
        graphicStockRessources = new Resource(254, 656, 124);
        
        
        initComponents();
        
        // Ruft nach 1 Sek. und alle 8 Sek. updateValues() auf, um die relevanten Werte zu aktualisieren
        Timer timer = new Timer();
        class test extends TimerTask {
            @Override
            public void run() {
                updateValues();
            }
        }
        test t1 = new test();
        timer.schedule(t1,500,500);
        
       
        
        
        
        // Erster Wert: x-Wert des klickbaren Feldes; zweiter Wert: y-Wert des klickbaren Feldes
        // Dritter Wert: x-Wert Ende des klickbaren; vierter Wert: y-Wert Ende des klickbaren
        // Fünfter Wert: Name für die Aktion; sechster Wert: Aktion/Methode
        //          
        //        
        // Dock Button 1
        buttons[0][0] = 329;
        buttons[1][0] = 617;
        buttons[2][0] = 441;
        buttons[3][0] = 714;
        actions[0][0] = "";
        actions[1][0] = "";
        
        // Dock Button 2
        buttons[0][1] = 510;
        buttons[1][1] = 617;
        buttons[2][1] = 623;
        buttons[3][1] = 714;
        actions[0][1] = "";
        actions[1][1] = "";
        
        // Dock Button 3
        buttons[0][2] = 682;
        buttons[1][2] = 617;
        buttons[2][2] = 795;
        buttons[3][2] = 714;
        actions[0][2] = "Troops";
        actions[1][2] = "openWindow";
       
        // Dock Button 4
        buttons[0][3] = 858;
        buttons[1][3] = 617;
        buttons[2][3] = 970;
        buttons[3][3] = 714;
        actions[0][3] = "Buildings";
        actions[1][3] = "openWindow";
        
        // Seitenleiste Button 1
        buttons[0][4] = 1189;
        buttons[1][4] = 326;
        buttons[2][4] = 1276;
        buttons[3][4] = 389;
        actions[0][4] = "Aufbereiter";
        actions[1][4] = "changeBuildingInfo";
        
        // Seitenleiste Button 2
        buttons[0][5] = 1194;
        buttons[1][5] = 393;
        buttons[2][5] = 1277;
        buttons[3][5] = 455;
        actions[0][5] = "Hauptgebäude";
        actions[1][5] = "changeBuildingInfo";
        
        // Seitenleiste Button 3
        buttons[0][6] = 1194;
        buttons[1][6] = 461;
        buttons[2][6] = 1277;
        buttons[3][6] = 520;
        actions[0][6] = "Raffinerie";
        actions[1][6] = "changeBuildingInfo";
        
        // Seitenleiste Button 4
        buttons[0][7] = 1194;
        buttons[1][7] = 526;
        buttons[2][7] = 1277;
        buttons[3][7] = 586;
        actions[0][7] = "Lager";
        actions[1][7] = "changeBuildingInfo";
        
        // Seitenleiste Button 5
        buttons[0][8] = 1194;
        buttons[1][8] = 592;
        buttons[2][8] = 1277;
        buttons[3][8] = 652;
        actions[0][8] = "Mine";
        actions[1][8] = "changeBuildingInfo";
       
        // Seitenleiste Button 6
        buttons[0][9] = 1194;
        buttons[1][9] = 658;
        buttons[2][9] = 1277;
        buttons[3][9] = 717;
        actions[0][9] = "Fabrik";
        actions[1][9] = "changeBuildingInfo";
       
    }
    
    

    public void updateValues() {
        try {
            // Soll die Werte regelmäßig updaten, die sich unregelmäßig verändern können
            Resource res = cm.getStockRessource(session);
            System.out.println("" + session);
            drawRessources(res);
            // lblName.setText(graphicBuildingLevel);
        } catch (Exception ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    
    public void paint(Graphics g) {
        this.g = g;
        this.g2 = (Graphics2D) g;
        
        
        super.paintComponents(g);
        //zeichnet Hintergrund
        g2.drawImage(img1, 0, 23, this);
        
        //Zeichnet Ressourcen
        drawBuildingInfo();
    }
    
    private void drawRessources(Resource res){ 
        lblBaustoff.setOpaque(true);
        lblBaustoff.setBackground(grey_topbar);
        lblMineralstoff.setOpaque(true);
        lblMineralstoff.setBackground(grey_topbar);
        lblTreibstoff.setOpaque(true);
        lblTreibstoff.setBackground(grey_topbar);
        
        lblMineralstoff.setText((int) res.mineral+"");
        lblTreibstoff.setText((int) res.treibstoff+"");
        lblBaustoff.setText((int) res.baustoff+"");
        
        lblBaustoff.repaint();
        lblMineralstoff.repaint();
        lblTreibstoff.repaint();
    }
    
    
    
    private void drawBuildingInfo(){
        
        lblName.setText(graphicBuildingName);      
        lblLevel.setText("Ausbaustufe: "+graphicBuildingLevel);
        lblInfo.setText("<html>"+graphicBuildingInfo+"</html>");
        

        lblName.setBackground(grey_sidebar);
        lblName.setOpaque(true);
        lblName.repaint();
        
        lblInfo.setOpaque(true);
        lblInfo.setBackground(grey_sidebar);
        lblLevel.setOpaque(true);
        lblLevel.setBackground(grey_sidebar);
        
        lblLevel.repaint();
        lblInfo.repaint();
        
    }
    
    
    
    
    public void changeRessources(Resource res) {        
        // Graphics2D g2 = (Graphics2D) g;        
        // g.drawString("Treibstoff: "+treibstoff+" Mineralstoff: "+mineralstoff+" Baustoff: "+baustoff, 548, 44);        

        graphicStockRessources = res;   
    }
    
    public void changeBuildingInfo(String name, String level, String info) {
        graphicBuildingName = name;
        graphicBuildingInfo = info;
        graphicBuildingLevel = level;

        drawBuildingInfo();
       
    }
    
    public String getSession() {
        return session;
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblName = new javax.swing.JLabel();
        lblTreibstoff = new javax.swing.JLabel();
        lblBaustoff = new javax.swing.JLabel();
        lblInfo = new javax.swing.JLabel();
        lblLevel = new javax.swing.JLabel();
        lblMineralstoff = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1280, 720));
        setResizable(false);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                catchClick(evt);
            }
        });
        getContentPane().setLayout(null);

        lblName.setFont(new java.awt.Font("Helvetica", 1, 16)); // NOI18N
        lblName.setForeground(java.awt.Color.white);
        lblName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblName.setText("Name");
        getContentPane().add(lblName);
        lblName.setBounds(1040, 50, 140, 20);

        lblTreibstoff.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblTreibstoff.setForeground(java.awt.Color.white);
        lblTreibstoff.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTreibstoff.setText("treib");
        getContentPane().add(lblTreibstoff);
        lblTreibstoff.setBounds(738, 31, 50, 16);

        lblBaustoff.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblBaustoff.setForeground(new java.awt.Color(255, 255, 255));
        lblBaustoff.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblBaustoff.setText("bau");
        getContentPane().add(lblBaustoff);
        lblBaustoff.setBounds(530, 31, 50, 16);

        lblInfo.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblInfo.setForeground(java.awt.Color.white);
        lblInfo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblInfo.setText("Info");
        lblInfo.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        getContentPane().add(lblInfo);
        lblInfo.setBounds(1040, 133, 220, 70);

        lblLevel.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblLevel.setForeground(java.awt.Color.white);
        lblLevel.setText("Level");
        getContentPane().add(lblLevel);
        lblLevel.setBounds(1040, 73, 140, 40);

        lblMineralstoff.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblMineralstoff.setForeground(java.awt.Color.white);
        lblMineralstoff.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMineralstoff.setText("mineral");
        getContentPane().add(lblMineralstoff);
        lblMineralstoff.setBounds(639, 31, 50, 20);

        jLabel1.setText("jLabel1");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 350, 280, 16);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void catchClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_catchClick
        Point mouseLocation = getMousePosition();
        
        int x = (int) mouseLocation.getX();
        int y = (int) mouseLocation.getY();
        // jLabel1.setText("Breite: "+x +"\n Höhe: "+y);
        
        for(int i=0;i<num_buttons;i++) {
            if(x >= buttons[0][i] && x <= buttons[2][i]
                    && y >= buttons[1][i] && y <= buttons[3][i]) {
               
                
                if(actions[1][i] == "openWindow") {
                    String handler = actions[0][i];
                    
                    if(handler == "CreateAccount") {
                        CreateAccount window = new CreateAccount();
                        window.setVisible(true);
                    }
//                    if(handler == "Trading") {
//                        About window = new About();
//                        window.setVisible(true);
//                    }
                    if(handler == "Troops") {
                        Troops window = new Troops(session, sc);
                        window.setVisible(true);
                    }
                    if(handler == "Buildings") {
                        Buildings window = new Buildings(session, sc);
                        window.setVisible(true);
                        
                        
                    }
                
                }
                else if(actions[1][i] == "changeBuildingInfo") {
                    try {
                        String name="", level="", info="";
                        int[] levels = cm.getMyBuildingLevels(session);
                        switch(actions[0][i]) {
                            case "Hauptgebäude":
                                name = "Nexus";
                                level = levels[0]+"";
                                info = "Das Hauptgebäude. Von hier aus können verschiedene Gebäude gesteuert werden.";
                                break;
                            case "Mine":
                                name = "Mine";
                                level = levels[1]+"";
                                info = "Die Mine produziert Mineralstoff. Der Ausbau beschleunigt die Produktion.";
                                break;
                            case "Raffinerie":
                                name = "Raffinerie";
                                level = levels[2]+"";
                                info = "Die Raffinere produziert Treibstoff. Der Ausbau beschleunigt die Produktion.";
                                break;
                            case "Aufbereiter":
                                name = "Aufbereiter";
                                level = levels[3]+"";
                                info = "Der Aufbereiter produziert Baustoffe";
                                break;
                            case "Lager":
                                name = "Lagerhalle";
                                level = levels[4]+"";
                                info = "Die Lagerhalle, um Ressourcen zu speichern. Der Ausbau erhöht die Kapazität.";
                                break;
                            case "Handelsrelais":
                                name = "Handelsrelais";
                                level = levels[5]+"";
                                info = "Der Handelsbereich";
                                break;
                            case "Labor":
                                name = "Labor";
                                level = levels[6]+"";
                                info = "Das Labor";
                                break;    
                            case "Fabrik":
                                name = "Fabrik";
                                level = levels[7]+"";
                                info = "Die Fabrik produziert verschiedene Einheiten. Der Ausbau beschleunigt die Produktion.";
                                break;
                            case "Schildgenerator":
                                name = "Schildgenerator";
                                level = levels[8]+"";
                                info = "Der Schildgenerator";
                                break;
                        }   changeBuildingInfo(name, level, info);
                    } catch (Exception ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                else {

                }
            }
        
        }    
    }//GEN-LAST:event_catchClick
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblBaustoff;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblLevel;
    private javax.swing.JLabel lblMineralstoff;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblTreibstoff;
    // End of variables declaration//GEN-END:variables
}