package frontend;

import backend.networking.AccountManager;
import backend.networking.ColonyManager;
import backend.networking.ServerConnection;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

public class Buildings extends javax.swing.JFrame {

    private String session="";
    private JTextField[] jtfs= new JTextField[10];
    private AccountManager am;
    private ColonyManager cm;
    
    public Buildings(String session, ServerConnection sc) {
        try {
            am = new AccountManager(sc);
            cm = new ColonyManager(sc);
            initComponents();
            
            this.session = session;
            
            jtfs[0] = txfB1;
            jtfs[1] = txfB2;
            jtfs[2] = txfB3;
            jtfs[3] = txfB4;
            jtfs[4] = txfB5;
            jtfs[5] = txfB6;
            jtfs[6] = txfB7;
            jtfs[7] = txfB8;
            jtfs[8] = txfB9;
            
            updateLevels();
        } catch (IOException ex) {
            Logger.getLogger(Buildings.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void updateLevels() {
        try {
            int[] levels = cm.getMyBuildingLevels(session);
            
            
            for(int i=0;i<9;i++) {
                jtfs[i].setText(""+levels[i]);
            }
        }
        catch(Exception e) {
            System.err.println(e);}
    }
    
    public void conTasks() {
        try {
            String[][] cons = cm.getMyConstructionTasks(session);
            for(int i=0; i<5;i++) {
                for(int j=0; j<=5;j++) {
                    System.out.println("x"+cons[i][j]+"");
                }
            }
        }
        catch(Exception e) {}  
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        lblB1 = new javax.swing.JLabel();
        txfB1 = new javax.swing.JTextField();
        lblCurrentAmount = new javax.swing.JLabel();
        btnB1 = new javax.swing.JButton();
        lblB2 = new javax.swing.JLabel();
        txfB2 = new javax.swing.JTextField();
        btnB2 = new javax.swing.JButton();
        lblB3 = new javax.swing.JLabel();
        txfB3 = new javax.swing.JTextField();
        btnB3 = new javax.swing.JButton();
        lblB4 = new javax.swing.JLabel();
        txfB4 = new javax.swing.JTextField();
        btnB4 = new javax.swing.JButton();
        lblExpand = new javax.swing.JLabel();
        btnB5 = new javax.swing.JButton();
        lblB5 = new javax.swing.JLabel();
        txfB5 = new javax.swing.JTextField();
        btnB6 = new javax.swing.JButton();
        lblB6 = new javax.swing.JLabel();
        txfB6 = new javax.swing.JTextField();
        btnB7 = new javax.swing.JButton();
        lblB7 = new javax.swing.JLabel();
        txfB7 = new javax.swing.JTextField();
        btnB8 = new javax.swing.JButton();
        lblB8 = new javax.swing.JLabel();
        txfB8 = new javax.swing.JTextField();
        btnB9 = new javax.swing.JButton();
        lblB9 = new javax.swing.JLabel();
        txfB9 = new javax.swing.JTextField();

        setAlwaysOnTop(true);

        lblTitle.setFont(new java.awt.Font("Helvetica", 1, 18)); // NOI18N
        lblTitle.setText("Nexus: Gebäude ausbauen");

        lblB1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB1.setText("Nexus");

        txfB1.setEditable(false);
        txfB1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB1.setText("0");
        txfB1.setFocusable(false);

        lblCurrentAmount.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblCurrentAmount.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCurrentAmount.setText("<html>aktuelle Ausbaustufe</html>");

        btnB1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB1.setText("+");
        btnB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB1ActionPerformed(evt);
            }
        });

        lblB2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB2.setText("Mine");

        txfB2.setEditable(false);
        txfB2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB2.setText("0");
        txfB2.setFocusable(false);

        btnB2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB2.setText("+");
        btnB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB2ActionPerformed(evt);
            }
        });

        lblB3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB3.setText("Raffinerie");

        txfB3.setEditable(false);
        txfB3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB3.setText("0");
        txfB3.setFocusable(false);

        btnB3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB3.setText("+");
        btnB3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB3ActionPerformed(evt);
            }
        });

        lblB4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB4.setText("Aufbereiter");

        txfB4.setEditable(false);
        txfB4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB4.setText("0");
        txfB4.setFocusable(false);

        btnB4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB4.setText("+");
        btnB4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB4ActionPerformed(evt);
            }
        });

        lblExpand.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblExpand.setText("ausbauen");

        btnB5.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB5.setText("+");
        btnB5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB5ActionPerformed(evt);
            }
        });

        lblB5.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB5.setText("Lagerhalle");

        txfB5.setEditable(false);
        txfB5.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB5.setText("0");
        txfB5.setFocusable(false);

        btnB6.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB6.setText("+");
        btnB6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB6ActionPerformed(evt);
            }
        });

        lblB6.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB6.setText("Handelsrelais");

        txfB6.setEditable(false);
        txfB6.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB6.setText("0");
        txfB6.setFocusable(false);

        btnB7.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB7.setText("+");
        btnB7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB7ActionPerformed(evt);
            }
        });

        lblB7.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB7.setText("Labor");

        txfB7.setEditable(false);
        txfB7.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB7.setText("0");
        txfB7.setFocusable(false);

        btnB8.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB8.setText("+");
        btnB8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB8ActionPerformed(evt);
            }
        });

        lblB8.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB8.setText("Fabrik");

        txfB8.setEditable(false);
        txfB8.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB8.setText("0");
        txfB8.setFocusable(false);

        btnB9.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnB9.setText("+");
        btnB9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnB9ActionPerformed(evt);
            }
        });

        lblB9.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblB9.setText("Schildgenerator");

        txfB9.setEditable(false);
        txfB9.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfB9.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfB9.setText("0");
        txfB9.setFocusable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitle)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(lblCurrentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(lblExpand))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblB5)
                            .addGap(48, 48, 48)
                            .addComponent(txfB5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(btnB5, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblB6)
                            .addGap(48, 48, 48)
                            .addComponent(txfB6, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(btnB6, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblB7)
                            .addGap(48, 48, 48)
                            .addComponent(txfB7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(btnB7, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblB8)
                            .addGap(48, 48, 48)
                            .addComponent(txfB8, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(btnB8, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblB2)
                                        .addComponent(lblB1))
                                    .addGap(54, 54, 54)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txfB1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txfB2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(lblB3)
                                        .addComponent(lblB4))
                                    .addGap(48, 48, 48)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txfB4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txfB3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(52, 52, 52)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnB4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnB3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(btnB1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnB2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lblB9)
                            .addGap(48, 48, 48)
                            .addComponent(txfB9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(btnB9, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(lblTitle)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(lblCurrentAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblExpand)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB1)
                    .addComponent(txfB1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB2)
                    .addComponent(txfB2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB3)
                    .addComponent(txfB3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB4)
                    .addComponent(txfB4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB5)
                    .addComponent(txfB5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB6)
                    .addComponent(txfB6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB7)
                    .addComponent(txfB7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB8)
                    .addComponent(txfB8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB9)
                    .addComponent(txfB9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnB9))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB1ActionPerformed
        try {
            cm.uprageBuilding(session, 0);
        }
        catch(Exception e) {
        
        }
    }//GEN-LAST:event_btnB1ActionPerformed

    private void btnB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB2ActionPerformed
       try {
            cm.uprageBuilding(session, 1);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB2ActionPerformed

    private void btnB3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB3ActionPerformed
        try {
            cm.uprageBuilding(session, 2);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB3ActionPerformed

    private void btnB4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB4ActionPerformed
       try {
            cm.uprageBuilding(session, 3);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB4ActionPerformed

    private void btnB5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB5ActionPerformed
        try {
            cm.uprageBuilding(session, 4);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB5ActionPerformed

    private void btnB6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB6ActionPerformed
        try {
            cm.uprageBuilding(session, 5);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB6ActionPerformed

    private void btnB7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB7ActionPerformed
        try {
            cm.uprageBuilding(session, 6);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB7ActionPerformed

    private void btnB8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB8ActionPerformed
        try {
            cm.uprageBuilding(session, 7);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB8ActionPerformed

    private void btnB9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnB9ActionPerformed
        try {
            cm.uprageBuilding(session, 9);
        }
        catch(Exception e) {}
    }//GEN-LAST:event_btnB9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Buildings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Buildings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Buildings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Buildings.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                // new Buildings().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnB1;
    private javax.swing.JButton btnB2;
    private javax.swing.JButton btnB3;
    private javax.swing.JButton btnB4;
    private javax.swing.JButton btnB5;
    private javax.swing.JButton btnB6;
    private javax.swing.JButton btnB7;
    private javax.swing.JButton btnB8;
    private javax.swing.JButton btnB9;
    private javax.swing.JLabel lblB1;
    private javax.swing.JLabel lblB2;
    private javax.swing.JLabel lblB3;
    private javax.swing.JLabel lblB4;
    private javax.swing.JLabel lblB5;
    private javax.swing.JLabel lblB6;
    private javax.swing.JLabel lblB7;
    private javax.swing.JLabel lblB8;
    private javax.swing.JLabel lblB9;
    private javax.swing.JLabel lblCurrentAmount;
    private javax.swing.JLabel lblExpand;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTextField txfB1;
    private javax.swing.JTextField txfB2;
    private javax.swing.JTextField txfB3;
    private javax.swing.JTextField txfB4;
    private javax.swing.JTextField txfB5;
    private javax.swing.JTextField txfB6;
    private javax.swing.JTextField txfB7;
    private javax.swing.JTextField txfB8;
    private javax.swing.JTextField txfB9;
    // End of variables declaration//GEN-END:variables
}
