package frontend;


import backend.networking.ColonyManager;
import backend.networking.ServerConnection;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Troops extends javax.swing.JFrame {

    private String session="";
    private ColonyManager cm;
    
    public Troops() {
        initComponents();
    }
    
    public Troops(String session, ServerConnection sc) {
        try {
            this.session = session;
            cm = new ColonyManager(sc);
            initComponents();
            
            
            
            
            txfTroop1.setText(null);
            txfTroop2.setText(null);
            txfTroop3.setText(null);
            txfTroop4.setText(null);
        } catch (IOException ex) {
            Logger.getLogger(Troops.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        lblTroop1 = new javax.swing.JLabel();
        txfTroop1 = new javax.swing.JTextField();
        lblCurrentAmount = new javax.swing.JLabel();
        btnTroop1 = new javax.swing.JButton();
        lblTroop2 = new javax.swing.JLabel();
        txfTroop2 = new javax.swing.JTextField();
        btnTroop2 = new javax.swing.JButton();
        lblTroop3 = new javax.swing.JLabel();
        txfTroop3 = new javax.swing.JTextField();
        btnTroop3 = new javax.swing.JButton();
        lblTroop4 = new javax.swing.JLabel();
        txfTroop4 = new javax.swing.JTextField();
        btnTroop4 = new javax.swing.JButton();

        setAlwaysOnTop(true);

        lblTitle.setFont(new java.awt.Font("Helvetica", 1, 18)); // NOI18N
        lblTitle.setText("Truppen rekrutieren");

        lblTroop1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblTroop1.setText("Truppentyp 1");

        txfTroop1.setEditable(false);
        txfTroop1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfTroop1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfTroop1.setText("0");
        txfTroop1.setFocusable(false);

        lblCurrentAmount.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblCurrentAmount.setText("aktuelle Anzahl");

        btnTroop1.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnTroop1.setText("+");
        btnTroop1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroop1ActionPerformed(evt);
            }
        });

        lblTroop2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblTroop2.setText("Truppentyp 2");

        txfTroop2.setEditable(false);
        txfTroop2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfTroop2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfTroop2.setText("0");
        txfTroop2.setFocusable(false);

        btnTroop2.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnTroop2.setText("+");
        btnTroop2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroop2ActionPerformed(evt);
            }
        });

        lblTroop3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblTroop3.setText("Truppentyp 3");

        txfTroop3.setEditable(false);
        txfTroop3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfTroop3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfTroop3.setText("0");
        txfTroop3.setFocusable(false);

        btnTroop3.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnTroop3.setText("+");
        btnTroop3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroop3ActionPerformed(evt);
            }
        });

        lblTroop4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        lblTroop4.setText("Truppentyp 4");

        txfTroop4.setEditable(false);
        txfTroop4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        txfTroop4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txfTroop4.setText("0");
        txfTroop4.setFocusable(false);

        btnTroop4.setFont(new java.awt.Font("Helvetica", 0, 15)); // NOI18N
        btnTroop4.setText("+");
        btnTroop4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroop4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTitle)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTroop1)
                        .addGap(48, 48, 48)
                        .addComponent(txfTroop1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnTroop1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(lblCurrentAmount))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTroop2)
                        .addGap(48, 48, 48)
                        .addComponent(txfTroop2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnTroop2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTroop3)
                        .addGap(48, 48, 48)
                        .addComponent(txfTroop3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnTroop3, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTroop4)
                        .addGap(48, 48, 48)
                        .addComponent(txfTroop4, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(52, 52, 52)
                        .addComponent(btnTroop4, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(lblTitle)
                .addGap(14, 14, 14)
                .addComponent(lblCurrentAmount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTroop1)
                    .addComponent(txfTroop1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTroop1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTroop2)
                    .addComponent(txfTroop2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTroop2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTroop3)
                    .addComponent(txfTroop3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTroop3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTroop4)
                    .addComponent(txfTroop4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTroop4))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTroop1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroop1ActionPerformed
        try {
            cm.recruteTroops(session, 1, 1);
        }
        catch (Exception ex) {
            Logger.getLogger(Troops.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTroop1ActionPerformed

    private void btnTroop2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroop2ActionPerformed
        try {
            cm.recruteTroops(session, 2, 1);
        }
        catch (Exception ex) {
            Logger.getLogger(Troops.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTroop2ActionPerformed

    private void btnTroop3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroop3ActionPerformed
        try {
            cm.recruteTroops(session, 3, 1);
        }
        catch (Exception ex) {
            Logger.getLogger(Troops.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTroop3ActionPerformed

    private void btnTroop4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroop4ActionPerformed
        try {
            cm.recruteTroops(session, 4, 1);
        }catch (Exception ex) {
            Logger.getLogger(Troops.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnTroop4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Troops.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Troops.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Troops.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Troops.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Troops().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnTroop1;
    private javax.swing.JButton btnTroop2;
    private javax.swing.JButton btnTroop3;
    private javax.swing.JButton btnTroop4;
    private javax.swing.JLabel lblCurrentAmount;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblTroop1;
    private javax.swing.JLabel lblTroop2;
    private javax.swing.JLabel lblTroop3;
    private javax.swing.JLabel lblTroop4;
    private javax.swing.JTextField txfTroop1;
    private javax.swing.JTextField txfTroop2;
    private javax.swing.JTextField txfTroop3;
    private javax.swing.JTextField txfTroop4;
    // End of variables declaration//GEN-END:variables
}
