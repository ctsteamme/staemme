package backend.networking;
import java.io.IOException;
import staemme_shared.*;

import static staemme_shared.Constants.*;


public class ColonyManager {

    private ServerConnection sc;
    
    public ColonyManager(ServerConnection sc) throws IOException {
        this.sc = sc;
    }

    
    
    
    
    /**
     * Liefert die allgemeinen (für jeden einsichtbaren) Informationen der Colony.
     * 1. Username
     * 2. Colonyname
     * 3. Points
     * 4. Position (in folgendem Format: xxx|yyy)
     * @param hash
     * @return
     */
    public String[] getMyColoniesInfo(String sessionHash) throws Exception{
        
        String answer = sc.sendRequest(COLONY_MANAGEMENT_GET_ColonyInfos + SPACE + sessionHash);
        String[] colonyInfos = new String[4];
        for (int i = 0; i < 4; i++) {
            colonyInfos[i] = answer.split(SPACE)[i+1];
        }
        return colonyInfos;     
    }
    
    
    
    
    /**
     * Liefer die zur Zeit im Besitz befindlichen Resourcen, als Resource Objekt
     * @param sessionHash
     * @return resource Objekt
     * @throws Exception 
     */
    public Resource getStockRessource(String sessionHash) throws Exception{
         System.out.println("getStockRessource" + sessionHash);
        Resource res = new Resource();
        
        String stringRessource = sc.sendRequest(COLONY_MANAGEMENT_GET_Stockressource + SPACE + sessionHash);
        
        res.decodeRes(stringRessource);
        
        return res;
    }
    
    
    
    /**
     * Liefert alle Gebäudestufen der Reihenfolge nacht.
     * public static final String[] buildingList = {"nexus", "mine", "raffinerie", "aufbereiter", 
     *   "lagerhalle", "handelsrelais", "labor", "fabrik", "schildgenerator"}; 
     * DIE REICHENFOLGE AUS DER SERVERCONFIG 
     * @param sessionHash
     * @return int[9] besitzt alle Level der Gebäudestufen
     * @throws Exception 
     */
    public int[] getMyBuildingLevels(String sessionHash) throws Exception {
        String bLevelString = sc.sendRequest(COLONY_MANAGEMENT_GET_Buildinglevels + SPACE + sessionHash);
        int[] buildingLevels = new int[9];
         for (int i = 0; i < buildingLevels.length; i++) {
             buildingLevels[i] = Integer.parseInt(bLevelString.split(SPACE)[i+1]);
         }
        
         return buildingLevels;
     }
     
    
   /**
     * Liefert die Infos für jeden Bauauftrag in Reihenfolge.
     * 1. GebäudeNr
     * 2. Zielstufe des Gebäudes
     * 3. Startzeit
     * 4. Fertigstellungszeit
     * 5. Verbleibende Dauer
     * @return 
     */
    public String[][] getMyConstructionTasks(String sessionHash) throws Exception{
        String constructionsString = sc.sendRequest(COLONY_MANAGEMENT_GET_ConstructionTasks + SPACE + sessionHash);
        String[] conParts = constructionsString.split(SPACE);
        int conSize = Integer.parseInt(conParts[1]);
        
        String[][] constructionTasks = null;
        
        for (int i = 0; i < conSize; i++) {
            for (int j = 0; j < 5; j++) {
                constructionTasks[i][j] += conParts[i + j + 3];
            }                                
        }      
        
        return constructionTasks;
        
    }    
      
    /**
     * Liefert die Infos für jeden Rekrutrierungsauftrag in Reihenfolge.
     * 1. Startzeit
     * 2. Fertigstellungszeit
     * 3. Verbleibende Dauer
     * 4. Truppentyp (als Name)
     * 5. Anzahl
     * @return 
     */
    public String[][] getMyRecrutionTasks(String sessionHash) throws Exception{
       String recrutionsString = sc.sendRequest(COLONY_MANAGEMENT_GET_RecrutionTasks + SPACE + sessionHash);
        String[] recParts = recrutionsString.split(SPACE);
        int recSize = Integer.parseInt(recParts[1]);
        
        String[][] constructionTasks = null;
        
        for (int i = 0; i < recSize; i++) {
            for (int j = 0; j < 5; j++) {
                constructionTasks[i][j] += recParts[i + j + 3];
            }         
        }    
        
        return constructionTasks;
        
    }
    
    
    
    /**
     * Liefert die 10 nächsten Colonies.
     * @param hash
     * @return 
     */    
    public String[][] getNearbyColonies(String sessionHash) throws Exception{
        
        String nearbyString = sc.sendRequest(COLONY_MANAGEMENT_GET_NearbyColonies + SPACE + sessionHash);
        String[] nearParts = nearbyString.split(SPACE);
        String[][] nearbyColonies = null;
        
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 4; j++) {
                nearbyColonies[i][j] = nearParts[i + j + 1];
            }
        }
        
        return nearbyColonies;
    }
    
    
    
    
    /**
     * Baut das gewünschte Gebäude aus
     * @param hash
     * @param buildingNr
     * @return 
     */
    public boolean uprageBuilding(String sessionHash, int buildingNr) throws Exception{
        
        String upgradeStatus = sc.sendRequest(COLONY_MANAGEMENT_SET_UpgradeBuilding + SPACE + sessionHash + SPACE + buildingNr);
        
        return Boolean.parseBoolean(upgradeStatus.split(SPACE)[1]);
    }
    
    
      
    /**
     * Rekrutiert die entsprechenden Truppen.
     * @param hash
     * @param troopTypeNr
     * @param amount
     * @return 
     */
    public boolean recruteTroops(String sessionHash, int troopTypeNr, int amount) throws Exception{
        String upgradeStatus = sc.sendRequest(COLONY_MANAGEMENT_SET_RecruteTroops + SPACE + sessionHash + SPACE + troopTypeNr + SPACE + amount);
        
        return Boolean.parseBoolean(upgradeStatus.split(SPACE)[1]);
    }  
    
    
}
 
