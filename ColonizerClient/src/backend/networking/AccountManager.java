package backend.networking;

import java.io.IOException;
import static staemme_shared.Constants.*;


public class AccountManager {
    
    ServerConnection sc;
    
    public AccountManager(ServerConnection sc) throws IOException {
        this.sc = sc;
    }

   private ServerConnection getSC() throws IOException{
       if(sc == null){
           sc = new ServerConnection();
       }
       return sc;
   }
    
    
    
    /**
     * 
     * @param password
     * @param username
     * @return boolean if login was successful
     * @throws Exception 
     */
    public boolean signUp(String username, String password) throws Exception{
        int requestType = ACCOUNT_MANAGEMENT_SIGNUP;
        String serverAnswer = sc.sendRequest(requestType + SPACE + username + SPACE + password);
        
        String[] saParts = serverAnswer.split(SPACE);
        String sessionID = "";
        //Überprüfe ob das eine SessionID sein sollte. Vielleicht unnötig
        if(Integer.parseInt(saParts[0]) == ACCOUNT_SIGNUP){
            sessionID = saParts[1];
        }
        
        return Boolean.parseBoolean(sessionID);
    }
    
    
    
    /**
     * Login.
     * 
     * @return SessionHash
     */
    public String login(String username, String password) throws Exception{
        int requestType = ACCOUNT_MANAGEMENT_LOGIN;        
        String sessionID = sc.sendRequest(requestType + SPACE + username + SPACE + password);
//        
//        if(Integer.parseInt(sessionID.split("::")[0]) == ACCOUNT_SESSIONID){
//            sessionID = sessionID.split("::")[1];
//        }else{
//            sessionID = "0000000000000000000000000000000000000000000000";
//            System.err.println("FEHLER_FALSHE SERVER ANTWORT");
//        }
//        
        return sessionID.split(SPACE)[1];
    }
    
}
