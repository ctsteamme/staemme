package backend.networking;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ServerConnection {
    public Socket serverconnection;

    public ServerConnection() throws IOException {
        System.out.println("Try to connect with Server");
        
        if(serverconnection == null){        
        //Verbindung zu Port 14000 auf localhost aufbauen:
            connectToServer();
        }
        
        System.err.println("" + serverconnection.getInetAddress());
    }
    
    

    public String sendRequest(String msg) {
        DataInputStream dIn = null;    
        String returnValue = "";
        try {
            if(serverconnection.isClosed()){
                connectToServer();
                
            }
            System.out.println("TRY TO SEND: " + msg + " TO SERVER");
            dIn = new DataInputStream(serverconnection.getInputStream());
            DataOutputStream dOut = new DataOutputStream(serverconnection.getOutputStream());
            dOut.writeUTF(msg);
            dOut.flush();
            returnValue = dIn.readUTF();
            
        } catch (Exception ex) {}     
        return returnValue;
    }

    private void connectToServer() {
        try {
            serverconnection = new Socket ("localhost", 14000);
            System.out.println("VERBINDE ZU SERVER: " + serverconnection.isConnected());
        } catch (IOException ex) {
            Logger.getLogger(ServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void closeConnection(){
        try {
            serverconnection.close();
        } catch (IOException ex) {
            Logger.getLogger(ServerConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
