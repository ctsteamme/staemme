
package Objects;

import java.util.*;


public class Bauauftrag extends TimerTask {

    private int anzahl;
    private java.util.Date start;
    final long estimatedEnd;
    
    MainFrame f;
    
    public Bauauftrag(MainFrame f, int anzahl, long estimatedEnd) {
        this.anzahl = anzahl;
        Calendar cal = Calendar.getInstance();
        start = cal.getTime();
        System.out.println(this.toString() + '\t' + start);
        this.estimatedEnd = estimatedEnd;
        this.f = f;
    }
    
    public java.util.Date getStart() {
        return start;
    }
    
    @Override
    public void run() {
        if(anzahl > 0) {
            System.out.println(this + " " + Calendar.getInstance().getTime());
            anzahl--;
        }
        else {
            this.cancel();
            f.list.remove(this);
        }
    }
}
